<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\ItemType;
use App\Http\Resources\ItemTypes as ItemTypesResource;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'API', 'middleware' => 'auth:api'], function() {
    
    /**
     * Trash implementation
     */
    Route::get('items/trashed', 'ItemsController@trashed_items');
    Route::get('items/delete_trashed/{id}', 'ItemsController@delete_trashed');
    Route::get('items/restore_trashed/{id}', 'ItemsController@restore_trashed');
    Route::get('items/empty_trash', 'ItemsController@empty_trash');
    // Route::get('items')

    /**
     * Share implementation
     */
    Route::get('items/shared_items/{limit}/{offset}', 'ItemsController@shared_items');

    /**
     * Resources
     */
    Route::resource('items', 'ItemsController');
    Route::resource('users', 'UsersController');
    Route::resource('files', 'ItemFilesController');
    Route::resource('categories', 'ItemTypesController');
    Route::resource('countries', 'CountriesController');
    Route::resource('covers', 'ItemCoversController', ['only' => ['index']]);
    Route::resource('authors', 'AuthorsController', ['only' => ['index']]);
    Route::resource('subjects', 'SubjectsController', ['only' => ['index']]);
    Route::resource('tags', 'TagsController', ['only' => ['index']]);
    Route::post('items/{item}/cover', 'ItemCoversController@store')->name('cover');
    Route::put('items/{item}/cover', 'ItemCoversController@update')->name('cover');
    Route::delete('items/{item}/cover', 'ItemCoversController@destroy')->name('cover');

    /**
     * Notifications
     */
    Route::get('log_user_notification', 'LogUserNotificationsController@index');
    Route::get('flag_read/{log_user_notification_id}', 'LogUserNotificationsController@flag_read');
    Route::get('all_user_notification', 'LogUserNotificationsController@all_user_notification');
    Route::get('count_user_notification', 'LogUserNotificationsController@count_user_notification');

});