const path = require('path');

module.exports = {
    entry: {
        items: path.resolve(__dirname, 'resources/assets/js/react/items.js'),
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
        ]
    }
}
