(function() {
    // let ItemsReq = axios.create({
    //     baseURL: '/admin/items',
    //     timeout: 3000,
    // });

    $(document).on('ready', function() {
        $('.btn-delete').on('click', function(e) {
            e.preventDefault();
            let itemId = $(this).data('item-id');
            console.log(itemId);

            ItemsReq.delete('/' + itemId)
                .then(function(res) {
                    console.log(res)
                })
                .catch(function(err) {
                    console.log(err)
                })
        })
    })
})();
