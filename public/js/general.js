function notification(options) {
  var notice = new PNotify({
    title: options.title,
    text: options.text,
    icon: options.icon,
    type: options.type
  });

  notice.get().click(function() {
    notice.remove();
  });
}
