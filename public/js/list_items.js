$(document).ready(function(){

  var current_click = 1;
  var offset = (current_click - 1) * 5;
  var token = document.querySelector("meta[name='csrf-token']").content;
  const baseURL = document.querySelector('meta[name="base_url"]').content;

  function url_check()
  {
    var url = ['/admin/dashboard'];
    var regexs = [];
    // var regex = new RegExp(url);
    var text = window.location.href;
    for (var i = 0; i < url.length; i++)
    {
      regexs[i] = new RegExp(url[i]);
    }

    for (var i = 0; i < regexs.length; i++)
    {
      text.match(regexs[i]);
      if (text.match(regexs[i]))
      {
        request_list(0, 0);
      }
    }
  }

  function load_notification(page)
  {
    if (page === undefined || null)
    {
      page = baseURL + "/api/all_user_notification";
    }
    var url = '/admin/notifications';
    var regex = new RegExp(url);
    var current_url = window.location.href;
    if (current_url.match(regex))
    {
      $.ajax({
        url : page,
        method : "GET",
        headers : {
          'X-CSRF-TOKEN' : token
        },
        success : function(resp){
          // console.log(resp);
          $.each(resp.data, function(i, val){
            // console.log(val);
            var action = [
              'none',
              'uploaded',
              'modified',
              'deleted'
            ];
            if (val.read == 0)
            {
              val.class = "unread";
            }

            val.title = (val.log_item.item == null) ? 'Item deleted permanently' : val.log_item.item.user.first_name + " has " + action[val.log_item.submit_action] + " " + val.log_item.item.title;
            val.title_min = (val.log_item.item == null) ? 'Item deleted permanently' : val.log_item.item.user.first_name + " has " + action[val.log_item.submit_action] + " " + limiter(val.log_item.item.title);
            val.photo = (val.log_item.item == null ||val.log_item.item.user.photo == null) ? baseURL + "/img/default-profile.jpg" : baseURL + "/storage/" + val.log_item.item.user.photo.download_path;
            val.time = moment(val.log_item.updated_at).fromNow();
            val.href = (val.log_item.item == null) ? 'javascript:void(0)' : baseURL + "/admin/items/"+val.log_item.item.id;
            if (moment().format('DD') - moment(val.log_item.updated_at).format('DD') < 0)
            {
              val.time = moment(val.log_item.updated_at).format('dddd, MMMM Do YYYY');
            }
          });

          $('#current_page').text(resp.current_page);
          $('#total').text(resp.last_page);

          $('#prev').data('link', resp.prev_page_url);
          $('#next').data('link', resp.next_page_url);
          $('#first').data('link', resp.first_page_url);
          $('#last').data('link', resp.last_page_url);
          $('#total-items').text(resp.total);

          if (resp.last_page > 1) {
            $('#paging').show();
          }

          if (resp.data.length > 0)
          {
            $('#list-notif').loadTemplate($('#tpl-notification'), resp.data);
          }
        }
      })
    }
  }

  $('div.btn-group .btn').click(function(){
    if ($(this).data('link'))
    {
      load_notification($(this).data('link'));
    }
  });

  $('div#paging div.btn-group .btn').click(function(){
    if ($(this).data('link'))
    {
      load_trash($(this).data('link'));
    }
  });

  function load_trash(URL)
  {
    if (URL === undefined) {
      URL = baseURL + "/api/items/trashed";
    }
    $('#list-trash').html('<h4><span class="fa fa-spinner fa-lg fa-spin"></span> Loading...</h4>')
    var url = '/admin/trash';
    var regex = new RegExp(url);
    var current_url = window.location.href;
    if (current_url.match(regex))
    {
      $.ajax({
        url : URL,
        headers : {
          'X-CSRF-TOKEN' : token
        },
        success : function(resp){
          $.each(resp.data, function(i, val){
            val.photo = val.cover == null ? baseURL + "/img/default-profile.jpg" : baseURL +  "/storage/" + val.cover.download_path;
            val.time = moment(val.deleted_at).fromNow();
          });

          $('#current_page').text(resp.meta.current_page);
          $('#total').text(resp.meta.last_page);

          $('#prev').data('link', resp.links.prev);
          $('#next').data('link', resp.links.next);
          $('#first').data('link', resp.links.first);
          $('#last').data('link', resp.links.last);

          $('#total-items').text(resp.meta.total);

          if (resp.meta.last_page > 1) {
            $('#paging').show();
          }

          if (resp.data.length > 0)
          {
            // $('#paging').show();
            $('#list-trash').loadTemplate($('#tpl-trash'), resp.data, {
              append : false
            });
            $('#open-dialog').attr('disabled', false);
          }
          else
          {
            $('#open-dialog').attr('disabled', true);
            $('#list-trash').empty();
            $('#list-trash').html('<h4><span class="fa fa-trash-o fa-lg"></span> No item in trash</h4>')
          }
        }
      });
    }
  }

  $(document).on('click', '.delete-trash-dialog', function(){
    var id = $(this).attr('id');
    $('#delete-item-dialog button.delete-trash').attr('id', id);
    $('#delete-item-dialog').modal('show');
  });

  $(document).on('click', '.delete-trash', function(){
    var id = $(this).attr('id');
    // console.log(id);
    $.ajax({
      url : baseURL + "/api/items/delete_trashed/" + id,
      headers : {
        "X-CSRF-TOKEN" : token
      },
      success : function(resp)
      {
        $('#delete-item-dialog').modal('hide');
        load_trash();
      }
    });
  });

  load_trash();
  load_notification();
  url_check();

  var items = 5;

  $('.load-files').click(function(){
    current_click++;
    items += 5;
    var total = parseInt($("#total-items").text());
    if (items > total)
    {
      items = total;
    }
    else
    {
      $('#items-show').text(items);
      offset = (current_click - 1) * 5;

      if (!$('#search-file').val())
      {
        request_list(offset, 0);
      }
      else
      {
        request_list(offset, $('#search-file').val());
      }
    }
  });

  $('#notification').click(function(e){
    e.stopPropagation();
    request();
    var req = new api();
    $('.notification-box').toggleClass('notif-in');
  });

  $(document).click(function(event){
    var $trigger = $(".notification-box");
    var trash = $('.trash-dialog');
    if($trigger !== event.target && !$trigger.has(event.target).length){
      $('.notification-box').removeClass('notif-in');
    }

    if (trash !== event.target && !trash.has(event.target).length)
    {
      trash.hide();
    }
  })

  $(document).on('click', '.notif-button',function(){
    $(this).parent('.notification-list').toggleClass('height');
    $(this).siblings('.title-min').toggleClass('visible');
    $(this).siblings('.title-ori').toggleClass('visible');
    $(this).children('i.glyphicon').toggleClass('glyphicon-chevron-down');
    $(this).children('i.glyphicon').toggleClass('glyphicon-chevron-up');
  });

  $('body').on('keyup', function(e){
    // console.log($('.notification-box').hasClass('notif-in'));
    if ($('.notification-box').hasClass('notif-in') && e.keyCode == 27 || $('.trash-dialog').css('display') == 'block' && e.keyCode == 27)
    {
      $('.notification-box').removeClass('notif-in');
      $('.trash-dialog').hide();
    }
  })

  // $('#notification').focusout(function(){
  //   $('.notification-box').removeClass('notif-in');
  // });

  $('#search-file').bind('input keyup', function(ev){
    // if (ev.keyCode == 8)
    // {
    //   if ($('#search-file').val() == '')
    //   {
    //     items = 5;
    //     $('#items-list').empty();
    //     $('#items-show').text(items);
    //     request_list(0, 0);
    //   }
    // }
    if ($('#search-file').val() != '')
    {
      if (ev.keyCode == 13)
      {
        $('#btn-back').show();
        items = 5;
        request_list(0, $('#search-file').val());
      }
    }
  });

  $('#btn-back').click(function(){
    $('#search-file').val('');
    $(this).hide();
    items = 5;
    $('#items-list').empty();
    $('#items-show').text(items);
    request_list(0, 0);
  })

  $(document).on('click', '.restore-trash', function(){
    $.ajax({
      type : "GET",
      url : baseURL + "/api/items/restore_trashed/" + $(this).attr('id'),
      headers : {
        'X-CSRF-TOKEN' : token
      },
      success : function(resp) {
        load_trash();
      }
    });
  });

  $('#btn-search-file').click(function(){
    // console.log($(this).parent().siblings('.form-control').val());
    if ($(this).parent().siblings('.form-control').val() != "")
    {
      request_list(0, $('#search-file').val());
    }
    else
    {
      $('#search-file').focus();
    }
  });

  function request_list(offset, search)
  {
    $('.loading').css('display', 'flex');
    $('.loading-msg').text('Loading...');
    if (offset === undefined)
    {
      offset = 0;
    }

    $.ajax({
      method : "GET",
      url : baseURL + "/api/items/shared_items/5/"+ offset,
      data : {
        q : $('#search-file').val()
      },
      headers : {
        'X-CSRF-TOKEN' : token
      },
      success : function(resp)
      {
        // console.log(resp);
        var total = (resp.total) ? resp.total : 0;
        $('.loading').hide();

        if (search == 0)
        {
          append_list(resp);
        }
        else if (offset >= 5 && items > 5)
        {
          append_list(resp);
        }
        else if (items < 5)
        {
          search_list(resp.items);
        }
        else
        {
          $("#items-list").empty();
          items = 5;
          search_list(resp.items);
        }

        $('#total-items').text(total);
        if (total > 5)
        {
          $('#items-show').text(items);
        }
        else
        {
          items = total;
          $('#items-show').text(total);
        }
      }
    });
  }

  $('#trash').contextmenu(function(e){
    // console.log('right click');
    $('.trash-dialog').toggle();
    e.preventDefault();
  });

  $(document).on('click','.visible',function(e){
    var id = $(this).attr('id');
    $.ajax({
      url : baseURL + "/api/flag_read/" + id,
      headers : {
        "X-CSRF-TOKEN" : token
      },
      success : function(resp)
      {
        var req = new api();
        request();
      }
    });
  });

  function append_list(resp)
  {
    var photo;
    $.each(resp.items, function(i, val){
      // console.log(val);
      val.uploader = val.uploader_first_name + " " + val.uploader_last_name;
      val.category = (val.category == null) ? "none" : val.category;
      val.href = baseURL + "/admin/items/" + val.id;
      val.title_min = limit_word(val.title);
      val.photo = (val.uploader_photo == null) ?  baseURL + '/img/default-profile.jpg' : (baseURL + '/storage/' + val.uploader_photo);
      val.date = moment(val.updated_at).format('dddd, MMMM Do YYYY');
    });
    $('#items-list').loadTemplate($('#tpl-list'), resp.items, {
      append: true
    });
  }

  $(document).on('click','.expand-list-item',function(){
    $(this).siblings('a.title-min').toggleClass('display');
    $(this).siblings('a.invisible').toggleClass('display');
    $(this).children('i.glyphicon').toggleClass('glyphicon-chevron-down');
    $(this).children('i.glyphicon').toggleClass('glyphicon-chevron-up');
  })

  function search_list(resp)
  {
    // if (resp.length == 0)
    // {
    $.each(resp, function(i, val){
      // console.log(val);
      val.uploader = val.uploader_first_name + " " + val.uploader_last_name;
      val.category = (val.category == null) ? "none" : val.category;
      val.href = baseURL + "/admin/items/" + val.id;
      val.title_min = limit_word(val.title);
      val.photo = (val.uploader_photo == null) ?  baseURL + '/img/default-profile.jpg' : (baseURL + '/storage/' + val.uploader_photo);
      val.date = moment(val.updated_at).format('dddd, MMMM Do YYYY');
    });
    // }
    if (resp.length > 0)
    {
      $('#items-list').loadTemplate($('#tpl-list'), resp, {
        append: false
      });
    }
    else
    {
      $("#items-list").empty();
      $("#items-list").append("<div style='text-align : center; padding : 4px 8px;'>File Not Found</div>")
    }
  }

  function limit_word(word)
  {
    // var str = word.split("");
    // console.log(word);
    if (word != undefined)
    {
      if (word.length > 30)
      {
        var strsplit = word.split("", 37);
        for (var i = 38; i < 41; i++)
        {
          strsplit[i] = '.';
        }
        word = strsplit.join("");
      }
    }
    return word;
  }

  /*
  *
  * Show items on dashboard admin/items/item_id
  *
  */

  var default_img = baseURL + "/img/default-profile.jpg";

  limiter = function(word)
  {
    if (word.length > 50)
    {
      var strsplit = word.split("", 47);
      for(var i = 48; i < 51; i++)
      {
        strsplit[i] = ".";
      }
      word = strsplit.join("");
    }

    return word;
  }

  var api = (function(){
    return function() {
      $.ajax({
        url : baseURL + "/api/log_user_notification",
        headers : {
          'X-CSRF-TOKEN' : token
        },
        success : function(resp) {
          $.each(resp, function(i, val){
            // console.log(val);
            var action = [
              'none',
              'uploaded',
              'modified',
              'deleted'
            ];
            if (val.read == 0)
            {
              val.class = "unread";
            }

            val.title = (val.log_item.item == null) ? 'Item deleted permanently' : val.log_item.item.user.first_name + " has " + action[val.log_item.submit_action] + " " + val.log_item.item.title;
            val.title_min = (val.log_item.item == null) ? 'Item deleted permanently' : val.log_item.item.user.first_name + " has " + action[val.log_item.submit_action] + " " + limiter(val.log_item.item.title);
            val.photo = (val.log_item.item == null ||val.log_item.item.user.photo == null) ? baseURL + "/img/default-profile.jpg" : baseURL + "/storage/" + val.log_item.item.user.photo.download_path;
            val.time = moment(val.log_item.updated_at).fromNow();
            val.href = (val.log_item.item == null) ? 'javascript:void(0)' : baseURL + "/admin/items/"+val.log_item.item.id;
            if (moment().format('DD') - moment(val.log_item.updated_at).format('DD') < 0)
            {
              val.time = moment(val.log_item.updated_at).format('dddd, MMMM Do YYYY');
            }
          })
          if (resp.length > 0)
          {
            $('.notification-container').loadTemplate($('#template'), resp);
          }
          //
          // })
          // console.log(resp);
        }
      })
    }

    // return this;
  })();

  $('.empty-trash').click(function(){
    $('.loading').css('display', 'flex');
    $('.loading-msg').text("Emptying trash...")
    $.ajax({
      type : "GET",
      url : baseURL + "/api/items/empty_trash",
      headers : {
        "X-CSRF-TOKEN" : token
      },
      success : function(resp)
      {
        if (resp.status)
        {
          setTimeout(function(){
            $('.loading').css('display', 'none');
          }, 500);
          $('.trash-dialog').hide();
          $('#empty-trash-dialog').modal('hide');
          load_trash();
        }
      }
    })
  })

  $('.notification-box').hover(function(){
    $('body').css('overflow', 'hidden');
  }, function(){
    $('body').css('overflow', 'auto');
  })

  function request()
  {
    $.ajax({
      url : baseURL + "/api/count_user_notification",
      headers : {
        "X-CSRF-TOKEN" : token
      },
      success : function(resp){
        $('.btn-header span div.badges').text(resp.total_unread);
        if (resp.total_unread == 0)
        {
          $('.btn-header span div.badges').hide();
        }
      }
    });
  }

  request();

  var req = new api();

  $('.cover-profile img').click(function(){
    let val = $(this).attr('src');
    var file = 'default.png';
    var regex = new RegExp(file);

    if (!val.match(regex))
    {
      $('#img-modal').modal('show');
      $('img#doc-img').attr('src', val);
    }
  })

  $('.item-files li a').click(function(e){
    var href = $(this).attr('href');
    var filename = $(this).text();
    var ext = 'pdf';
    var regex = new RegExp(ext);
    if (href.match(regex))
    {
      $('#pdf-modal').modal('show');
      $('#pdf-modal .modal-title').text(filename);
      $('#pdf-modal .modal-dialog .modal-body object').attr('data', href);
      e.preventDefault();
    }
  })

});
