(function() {
    let ItemFilesReq = axios.create({
        baseURL: '/api/files',
        timeout: 3000,
    });

    let MAX_FILE = 1;
    let FILE_TYPE = ['pdf', 'doc', 'docx', 'jpg', 'jpeg', 'JPG', 'JPEG', 'png', 'PNG', 'bmp', 'BMP']
    var uploadTokenSession = moment().unix();

    function updateUploadToken() {
        uploadTokenSession = moment().unix();
    }

    function ItemFiles() {
        this.itemList = [];

        this.fetchFiles = function() {
            let that = this;
            let itemFilesVal = $('input#item_files').val();
            console.log(itemFilesVal);
            if(itemFilesVal) {
                itemFilesId = (_.split(itemFilesVal, ','));
                console.log(itemFilesId);
                _.forEach(itemFilesId, function(fid) {
                    ItemFilesReq.get('/' + fid).then(function(r) {
                        console.log(r.data)
                        that.addFile(r.data);
                    }).catch(function(e) {
                        console.log(e);
                    })
                });

            }
        }

        this.addFile = function(file) {
            this.itemList.push(file);
            this.populateInputField();
            this.createItemFilePanel(file)
        };

        this.removeFile = function(id) {
            this.itemList = _.reject(this.itemList, function(o) {
                return o.id == id;
            })

            this.populateInputField();
            $('#item-files-list').find('#panel-item-file-' + id).remove();
        }

        this.populateInputField = function() {
            let el = $('input#item_files');
            let itemFilesId = _.join(_.map(this.itemList, function(o) {
                return o.id
            }), ',');

            el.val(itemFilesId);
        }

        this.createItemFilePanel = function(data) {
            let desc = data.description ? data.description : "no description.";
            let newPanel = $('<div class="panel panel-success panel-item-file" data-item-file-id="' + data.id + '" id="panel-item-file-' + data.id + '">\
                <div class="panel-heading">\
                    <a class="item-link item-file-title" target="_blank" href="/storage/' + data.download_path + '">' + data.filename + '</a>\
                    <i class="fa fa-trash pull-right btn-delete-item-file" aria-hidden="true" data-id="' + data.id + '"></i>\
                    <a class="item-link" href="/storage/' + data.download_path + '" target="_blank">\
                        <i class="fa fa-eye pull-right btn-view-item-file" aria-hidden="true" data-id="' + data.id + '"></i>\
                    </a>\
                </div>\
                <div class="panel-body">\
                    <p class="item-description">' + desc + '</p>\
                </div>\
            </div>')

            $('#item-files-list>.panel-body').append(newPanel);
        }
    }

    let itemFiles = new ItemFiles();
    itemFiles.fetchFiles();

    $(document).on('ready', function() {
        let fileUploader = new Resumable({
            headers: {
                'X-CSRF-TOKEN': token
            },
            simultaneousUploads: 1,
            generateUniqueIdentifier: function(file, event) {
                updateUploadToken();
                console.log("GENERATE UNIQE IDENTIFIER:")
                console.log("file:", file);
                console.log("event:", event);
                console.log("upload token session:", uploadTokenSession);
                return uploadTokenSession + "-" + file.size + "-" + file.name;
            },
            target: '/admin/item/files',
            fileType: FILE_TYPE,
            fileTypeErrorCallback(file, errorCount) {
                console.log("error on file:", file, "total error:", errorCount);
                var fileType = $.map(fileUploader.opts.fileType, function(e) { return "." + e }).join(" & ");
                notification({
                    title: 'Error file type',
                    text: 'Please upload correct file type!\nFile uploader only accepting ' + fileType,
                    icon: 'fa fa-times-circle',
                    type: 'error'
                })
            },
            maxFiles: MAX_FILE,
            query: function() {
                let explanationText = $('#item-file-description').val();
                console.log("fuck:", explanationText);
                let totalFiles = fileUploader.files.length;
                return {
                    'description': explanationText,
                    'tokenSession': uploadTokenSession
                }
            },
            maxFilesErrorCallback: function(files, errorCount) {
                console.log("number of files to upload exceeding max files!!");
                notification({
                    title: 'Warning max total files',
                    text: 'Number of files to upload exceeding max files!\n Max files to upload is ' +  fileUploader.opts.maxFiles + ' file(s)!',
                    icon: 'fa fa-times-circle',
                    type: 'warning'
                })
            },
            uploadMethod: 'POST',
            testChunks: false
        });

        fileUploader.assignBrowse(document.getElementById('btn-add-file'));

        fileUploader.on('fileAdded', function(file, event) {
            console.log(file, event);
            $('#file-name').val(file.fileName);
        });

        fileUploader.on('fileSuccess', function(file, message){
            $('#file-name').val("");
            $('#item-file-description').val('');
            $("#modal-item-uploader").modal('hide');

            notification({
                title: 'Upload Item File',
                text: 'File Uploaded',
                icon: 'fa fa-check-square-o',
                type: 'success'
            })

            let data = JSON.parse(message);
            itemFiles.addFile(data);
        });

        fileUploader.on('fileError', function(file, message){
            $('#file-name').val("");
            console.log(file, message);
        });

        fileUploader.on('fileRetry', function(file, message){
            console.log(file, message);
        });

        fileUploader.on('progress', function() {
            console.log('progress bro!');
        });

        fileUploader.on('complete', function() {
            console.log('complete');
        });

        fileUploader.on('uploadStart', function() {
            console.log('start');
        });

        fileUploader.on('fileProgress', function(file) {
        });

        $('#btn-upload-item-file').on("click", function(e) {
            e.preventDefault();
            fileUploader.upload();
        })

        $("#modal-item-uploader").on('shown.bs.modal', function() {
            //console.log(itemUploader);
        });

        $("#modal-item-uploader").on('hidden.bs.modal', function() {
            $('#item-file-description').val('');
            $('#file-name').val("");

            fileUploader.files.forEach(function(file) {
                fileUploader.removeFile(file);
            })
        });

        $('#show-modal-item-uploader').on('click', function(e) {
            e.preventDefault();
            $("#modal-item-uploader").modal('show');
        });


        var form = $('#form-create-item').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                "title": {
                    validators: {
                        notEmpty: {
                            message: "Title can't be empty"
                        }
                    }
                },
                "preview": {
                    validators: {
                        notEmpty: {
                            message: "Preview can't be empty"
                        }
                    }
                },
                "document_date": {
                    validators: {
                        notEmpty: {
                            message: "Can't be empty"
                        },
                        regexp: {
                            regexp: /^([0-9]{4}-[0-9]{2}-[0-9]{2})$/,
                            message: 'Document Date format must be YYYY-MM-DD'
                        }
                    }
                },
            }
        })


        $('#document_date').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '-3d'
        });
    });


    $('#item-files-list').on('click', '.btn-delete-item-file', function() {
        let el = $(this);
        let fileId = parseInt(el.data('id'));

        itemFiles.removeFile(fileId);
    })
})();
