<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemIdAndSubjectIdToItemSubjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_subjects', function($table) {
            $table->integer('item_id')->unsigned()->nullable();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('subject_id')->unsigned()->nullable();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_subjects', function($table) {
            $table->dropForeign('item_subjects_item_id_foreign');
            $table->dropColumn('subject_id');

            $table->dropForeign('item_subjects_subject_id_foreign');
            $table->dropColumn('item_id');
        });
    }
}
