<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(RoleTableSeeder::class);
        $this->call(CountryTableSeeder::class);
        $this->call(ItemTypeSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
