<?php

use Illuminate\Database\Seeder;
use App\Author;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list_author = ['Febrian Rendak', 'Dimz Nugroho', 'McDy', 'Andhee'];
        foreach($list_author as $name) {
            Author::create([
                'name' => $name
            ]);
        }
    }
}
