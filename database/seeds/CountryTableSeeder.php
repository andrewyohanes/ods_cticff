<?php

use Illuminate\Database\Seeder;
use App\Country;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::truncate();

        $country_list = [
            [
                'name'=> 'Indonesia',
                'code'=> 'ID'
            ],
            [
                'name'=> 'Malaysia',
                'code'=> 'MY'
            ],
            [
                'name'=> 'Papua New Guinea',
                'code'=> 'PNG'
            ],
            [
                'name'=> 'Philippines',
                'code'=> 'PH'
            ],
            [
                'name'=> 'Solomon Islands',
                'code'=> 'SL'
            ],
            [
                'name'=> 'Timor Leste',
                'code'=> 'TL'
            ]
        ];

        foreach($country_list as $country) {
            $c = new Country();
            $c->name = $country['name'];
            $c->code = $country['code'];
            $c->save();
        }
    }
}
