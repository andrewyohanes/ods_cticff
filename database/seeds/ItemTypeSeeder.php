<?php

use Illuminate\Database\Seeder;
use App\ItemType;

class ItemTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        ItemType::truncate();

        $item_types = [
            "Reports and Studies",
            "Policies and Agreements",
            "Event Materials",
            "Outreach Materials",
            "Training Materials",
            "Statistics and Data Sets",
            "Photos, Videos, and Maps"
        ];

        foreach($item_types as $item_type) {
            $it = new ItemType();
            $it->name = $item_type;
            $it->save();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
