@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">Add New Country</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" class="form" action="{{ route('countries.store') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control"value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="code">Country Code</label>
                        <input type="text" id="code" name="code" class="form-control"value="{{ old('code') }}">
                    </div>
                    <button type="submit" class="btn btn-md btn-primary pull-right">Create</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
