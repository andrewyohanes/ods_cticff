@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Country List
                @if(Auth::user()->isAdmin() || Auth::user()->isRoot())
                <a class="btn btn-success btn-xs pull-right" href="{{route('countries.create')}}">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Country
                </a>
                @endif
            </div>

            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Country Code</th>
                            <th>Items</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($countries as $country)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $country->name }}</td>
                            <td>{{ $country->code }}</td>
                            <td>{{ $country->items->count() }}</td>
                            <td>
                                @if(Auth::user()->isAdmin() || Auth::user()->isRoot())
                                <a href="{{route('countries.edit', [$country->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{route('countries.delete', [$country->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
