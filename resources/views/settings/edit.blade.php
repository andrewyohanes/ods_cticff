@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">User Setting</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" class="form" action="{{ route('setting') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="password">Old Password</label>
                        <input type="password" id="password" name="password" class="form-control"value="{{ old('password') }}">
                    </div>
                    <div class="form-group">
                        <label for="new_password">New Password</label>
                        <input type="password" id="password_new" name="password_new" class="form-control" value="{{ old('password_new') }}">
                    </div>
                    <div class="form-group">
                        <label for="password_new_confirmation">New Password Confirm</label>
                        <input type="password" id="password_new_confirmation" name="password_new_confirmation" class="form-control" value="{{ old('password_new_confirmation') }}">
                    </div>
                    <button type="submit" class="btn btn-md btn-primary pull-right">SAVE</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
