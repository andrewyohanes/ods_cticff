@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Author List
                @if(Auth::user()->isAdmin() || Auth::user()->isRoot())
                <a class="btn btn-success btn-xs pull-right" href="{{route('authors.create')}}">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Author
                </a>
                @endif
            </div>

            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Total Items</th>
                            <th>Created at</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($authors as $author)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $author->name }}</td>
                            <td>{{ $author->items->count() }}</td>
                            <td>{{ $author->created_at }}</td>
                            <td>
                                @if(Auth::user()->isAdmin() || Auth::user()->isRoot())
                                <a href="{{route('authors.edit', [$author->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{route('authors.delete', [$author->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
