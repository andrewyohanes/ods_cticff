@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">
                Category List
                @if(Auth::user()->isAdmin() || Auth::user()->isRoot())
                <a class="btn btn-success btn-xs pull-right" href="{{route('categories.create')}}">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add Category </a>
                @endif
            </div>

            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Items</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($item_types as $item_type)
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $item_type->name }}</td>
                            <td>{{ $item_type->description }}</td>
                            <td>{{ $item_type->items->count() }}</td>
                            <td>{{ $item_type->created_at }}</td>
                            <td>{{ $item_type->updated_at }}</td>
                            <td>
                                @if(Auth::user()->isAdmin() || Auth::user()->isRoot())
                                <a href="{{route('categories.edit', [$item_type->id])}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{route('categories.delete', [$item_type->id])}}"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
