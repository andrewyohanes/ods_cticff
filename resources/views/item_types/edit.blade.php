@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">Edit Category</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" class="form" action="{{ route('categories.update', [$item_type->id]) }}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="PUT">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" name="name" class="form-control"value="{{ old('name', $item_type->name) }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea id="description" name="description" class="form-control">{{ old('description', $item_type->description) }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary pull-right">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
