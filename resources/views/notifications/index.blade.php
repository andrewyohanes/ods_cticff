@extends('layouts.main')

@section('content')
  <div class="well">
    <h1><i class="fa fa-bell fa-lg"></i>&nbsp;Notifications</h1>
    <hr>
    <div id="list-notif">
      <p style="text-align:center;">No notification</p>

    </div>
    {{-- <div class="notification">
      <div class="notif-img">
        <img src="/ods/public/storage/upload/3jxZPLGDAcwly2ZmzMsAmhSX7KgLdto8wWBUq5BX.jpeg" alt="">
      </div>
      <a href="#">Admin has modified Test notif dengan bla bla bla</a>
      <span>5 minutes ago</span>
    </div> --}}
    <hr>
    <div id="paging">
      <div class="btn-group">
        <button type="button" id="first" data-link="" class="btn btn-primary" name="button">First page</button>
        <button type="button" id="prev" data-link="" class="btn btn-default" name="button">
          <span class="fa fa-angle-left fa-lg"></span>&nbsp;Prev
        </button>
        <button type="button" id="next" data-link="" class="btn btn-default" name="button">
          Next&nbsp;<span class="fa fa-angle-right fa-lg"></span>
        </button>
        <button type="button" id="last" data-link="" class="btn btn-primary" name="button">Last page</button>
      </div>
      <code id="current_page">0</code>
      /
      <code id="total">0</code>
      pages
      <kbd>Total : <code id="total-items"></code> items</kbd>
    </div>
  </div>
  <script id="tpl-notification" type="text/html">
    <div class="notification">
      <div class="notif-img">
        <img data-src="photo" alt="log_item.item.user.first_name">
      </div>
      <a data-href="href" data-content="title"></a>
      <span data-content="time"></span>
    </div>
  </script>
@include('items/_modal_item_uploader')
@endsection
