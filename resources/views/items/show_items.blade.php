@extends('layouts.main')

@section('content')
<div class="well well-sm">
  <div class="row">
    <div class="col-sm-8">
      <button type="button" class="btn btn-info" name="button" id="btn-back"><i class="fa fa-chevron-left fa-lg"></i>&nbsp;Back</button>
    </div>
    <div class="col-sm-4">
      <div class="input-group">
        <input type="text" class="form-control" id="search-file" placeholder="Search your files" name="" value="">
        <div class="input-group-btn">
          <button type="button" class="btn btn-primary" id="btn-search-file" name="button"><i class="glyphicon glyphicon-search"></i>&nbsp;Search</button>
        </div>
        {{--  --}}
      </div>
      {{--  --}}
    </div>
    {{--  --}}
  </div>
  <hr>
  <div class="folder">
    <i class="glyphicon glyphicon-file"></i>&nbsp;Your Files
  </div>
  <ul id="items-list" class="list-items">

  </ul>
  <button class="load-files"><i class="glyphicon glyphicon-chevron-down"></i></button>
  <code id="items-show">0</code>/<code id="total-items">0</code> items
</div>
<script id="tpl-list" type="text/html">
  <li>
    <div class="img">
      <img data-src="photo" data-title="title"/>
    </div>
    <a data-href="href" class="title-min display" data-content-prepend="title_min">
      <span> Uploaded by <code data-content="uploader"></code> | Category : <code data-content="category"></code> <label data-content="date"></label></span>
    </a>
    <a data-href="href" class="invisible" data-content-prepend="title">
      <span> Uploaded by <code data-content="uploader"></code> | Category : <code data-content="category"></code><label data-content="date"></label></span>
    </a>
    <button href="javascript:void(0)" class="expand-list-item" title="Expand"><i class="glyphicon glyphicon-chevron-down"></i></button>
  </li>
</script>
{{-- <div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">Item Details</div>
            <div class="panel-body">
                <div class="panel panel-primary panel-item-files">
                    <div class="panel-heading">
                        Title
                    </div>
                    <div class="panel-body">
                        {{$item->title}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Alternative Title
                    </div>
                    <div class="panel-body">
                        {{$item->alternative_title}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Preview
                    </div>
                    <div class="panel-body">
                        {{$item->preview}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Summary
                    </div>
                    <div class="panel-body">
                        {{$item->summary}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Uploader
                    </div>
                    <div class="panel-body">
                        {{$item->uploaded_by->full_name()}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Cover Image
                    </div>
                    <div class="panel-body">
                        @if($item->cover)
                        <img src="{{asset('storage/'.$item->cover->download_path)}}" className="img-responsive" alt="image-review" />
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Category
                    </div>
                    <div class="panel-body">
                        {{$item->item_type ? $item->item_type->name : ''}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Subject
                    </div>
                    <div class="panel-body">
                        {{$item->subject ? $item->subject->name : ''}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Authors
                    </div>
                    <div class="panel-body">
                        <ul class="list">
                            @foreach($item->authors as $author)
                            <li>{{ $author->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Tags
                    </div>
                    <div class="panel-body">
                            @foreach($item->tags as $tag)
                            <div class="label label-success">{{ $tag->name }}</div>
                            @endforeach
                    </div>
                </div>

                <div class="panel panel-primary panel-item-item">
                    <div class="panel-heading">
                        Document Date
                    </div>
                    <div class="panel-body">
                        {{$item->document_date}}
                    </div>
                </div>

                <div class="panel panel-primary panel-item-files" id="item-files-list">
                    <div class="panel-heading">
                        Item Files
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                @foreach ($item->files as $file)
                                    <div class="panel panel-success">
                                        <div class="panel-heading">
                                            <a target="_blank" href="/storage/{{$file->download_path}}">{{$file->filename}}</a>
                                        </div>
                                        <div class="panel-body">
                                            @if($file->description)
                                                {{$file->description}}
                                            @else
                                                no description...
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

@include('items/_modal_item_uploader')
@endsection
