@extends('layouts.main')

@section('content')

<div class="cover">
  <div class="img-cover">
    <img src="{{$item->cover ? asset('storage/'. $item->cover->download_path) : asset('img/default-cover.jpg')}}" class="img" alt=""/>
  </div>
  <div class="cover-profile">
    <img src="{{$item->cover ? asset('storage/'. $item->cover->download_path) : asset('img/default-cover.jpg')}}" title="{{ $item->cover ? 'Click to view' : 'Default cover' }}">
  </div>
</div>
<div class="title">
  <h1>{{$item->title}}</h1>
  <p>Alternative title : {{ $item->alternative_title ? $item->alternative_title : 'No Alternative title'}}</p>
  <p class="label label-success">Category : {{$item->item_type ? $item->item_type->name : 'Misc'}}</p>
  <label><i class="glyphicon glyphicon-calendar"></i>&nbsp;{{ $item->document_date }}</label>
  <h4>Uploaded by {{ $item->uploaded_by->full_name() }}</h4>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-7">
      <hr>
      <div class="input-group">
        <div class="input-group-addon">
          Subject
        </div>
        <input type="text" class="form-control" placeholder="Subject" value="{{$item->subject ? $item->subject->name : 'No subject'}}" readonly>
      </div>
<!--        -->
      <br>
      <p>Authors :
        @if ($item->authors)
          @foreach($item->authors as $author)
            <code>{{ $author->name }}</code>
          @endforeach
        @else
          <code>No author</code>
      @endif
      </p>
      <br>
      <p>Tags :
        @if (sizeof($item->tags) > 0)
          @foreach($item->tags as $tag)
            <code>{{ $tag->name }}</code>
          @endforeach
        @else
          <code>No tag</code>
        @endif
      </p>
      <br>
      <h4>Summary</h4>
      <p class="summary">{!! $item->summary ? $item->summary : 'No Summary' !!}</p>
      <br>
<!--        -->
    </div>
    <div class="col-sm-4">
      <hr>
      <h3><i class="glyphicon glyphicon-file"></i> Files</h3>
      <br>
      <ul class="item-files">
        @foreach ($item->files as $file)
          <li>
            {{-- {!! $file->filetype !!} --}}
            @if ($file->filetype == 'pptx' || $file->filetype == 'ppt')
              <span><i class="fa fa-file-powerpoint-o fa-lg"></i>&nbsp;{!! strtoupper($file->filetype) !!}</span>
            @elseif ($file->filetype == 'docx' || $file->filetype == 'doc')
              <span><i class="fa fa-file-word-o fa-lg"></i>&nbsp;{!! strtoupper($file->filetype) !!}</span>
            @elseif ($file->filetype == 'xlsx' || $file->filetype == 'xls')
              <span><i class="fa fa-file-excel-o fa-lg"></i>&nbsp;{!! strtoupper($file->filetype) !!}</span>
            @elseif ($file->filetype == 'pdf')
              <span><i class="fa fa-file-pdf-o fa-lg"></i>&nbsp;{!! strtoupper($file->filetype) !!}</span>
            @elseif ($file->filetype == 'png' || $file->filetype == 'jpeg' || $file->filetype == 'jpg')
              <span><i class="fa fa-file-image-o fa-lg"></i>&nbsp;{!! strtoupper($file->filetype) !!}</span>
            @else
              <span><i class="fa fa-file-o fa-lg"></i>&nbsp;{!! strtoupper($file->filetype) !!}</span>
            @endif
            <a target="_blank" href="{{URL::to('/')}}/storage/{{$file->download_path}}">{{$file->filename}}</a>
          </li>
        @endforeach
        {{-- <a href="#">File name</a> --}}
        {{-- <li><span><i class="fa fa-file-word-o fa-lg"></i></span><a href="#">File name 2</a> --}}
        </li>
      </ul>
    </div>
  </div>
</div>

<div class="modal fade" id="img-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
        <h4 class="modal-title">{{ $item->title }}</h4>
      </div>
      <div class="modal-body">
        <img src="" class="img img-responsive thumbnail" id="doc-img" alt="">
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="pdf-modal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" name="button">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <object type="application/pdf" data="" height="500" width="100%">

        </object>
      </div>
    </div>
  </div>
</div>

@include('items/_modal_item_uploader')
@endsection
