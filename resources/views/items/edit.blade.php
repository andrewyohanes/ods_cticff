@extends('layouts.main')


@section('customstyle')
@endsection

@section('content')
<div id="item-id" data-id="{{$item->id}}"></div>
<div id="app-root"></div>
@endsection

@section('customjs')
<script src="{{asset('vendors/moment/moment.js')}}"></script>
<script src="{{asset('js/item-edit.js')}}"></script>
@endsection
