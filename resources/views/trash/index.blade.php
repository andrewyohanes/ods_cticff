@extends('layouts.main')

@section('content')
  <div class="well">
    <h1><span class="fa fa-trash fa-lg"></span> Trash</h1>
    <button type="button" class="btn btn-danger btn-xs" disabled="disabled" id="open-dialog" data-toggle="modal" data-target="#empty-trash-dialog" name="button">Empty Trash</button>
    <hr>
    <div id="list-trash" class="text-center">
      <h4><span class="fa fa-spinner fa-lg fa-spin"></span> Loading...</h4>
    </div>
    <hr>
    <div id="paging">
      <div class="btn-group">
        <button type="button" id="first" data-link="" class="btn btn-primary" name="button">First page</button>
        <button type="button" id="prev" data-link="" class="btn btn-default" name="button">
          <span class="fa fa-angle-left fa-lg"></span>&nbsp;Prev
        </button>
        <button type="button" id="next" data-link="" class="btn btn-default" name="button">
          Next&nbsp;<span class="fa fa-angle-right fa-lg"></span>
        </button>
        <button type="button" id="last" data-link="" class="btn btn-primary" name="button">Last page</button>
      </div>
      <code id="current_page">0</code>
      /
      <code id="total">0</code>
      pages
      <kbd>Total : <code id="total-items"></code> items</kbd>
    </div>
  </div>
  <div class="modal fade" id="empty-trash-dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-trash-o fa-lg"></span> Empty trash</h4>
        </div>
        <div class="modal-body">
          <h1 class="text-center"><span class="fa fa-exclamation-circle fa-lg"></span>&nbsp;</h1>
          <p class="text-justify">Are you sure you want to permanently erase the items in the trash?</p>
          <code>You can’t undo this action.</code>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger btn-sm empty-trash">Empty Trash</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="delete-item-dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><span class="fa fa-trash-o fa-lg"></span> Delete item?</h4>
        </div>
        <div class="modal-body">
          <h1 class="text-center"><span class="fa fa-exclamation-circle fa-lg"></span>&nbsp;</h1>
          <p class="text-justify">Are you sure you want to permanently erase this item from the trash?</p>
          <code>You can’t undo this action.</code>
          <input type="hidden" id="item-id" name="" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
          <button type="button" id="" class="btn btn-danger btn-sm delete-trash">Delete item</button>
        </div>
      </div>
    </div>
  </div>
  <script id="tpl-trash" type="text/html">
    <div class="notification">
      <div class="notif-img">
        <img data-src="photo" alt="">
      </div>
      <a href="javascript:void(0)" data-content="title"></a>
      <span data-content-prepend="time">
        <button data-id="id" class="btn btn-success btn-xs restore-trash">Restore item</button>
        <button data-id="id" class="btn btn-danger btn-xs delete-trash-dialog">Delete from trash</button>
      </span>
    </div>
  </script>
@endsection
