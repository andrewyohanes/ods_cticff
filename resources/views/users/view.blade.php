@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">View User Details</div>

            <div class="panel-body">
                {{ $user->first_name }} {{ $user->last_name }}
            </div>
        </div>
    </div>
</div>
@endsection
