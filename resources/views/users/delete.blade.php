@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">Delete User</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" class="form" action="{{ route('users.destroy', [$user->id]) }}">
                    {{ csrf_field() }}
                    <input name="_method" type="hidden" value="DELETE">
                    <input name="id" type="hidden" value="{{$user->id}}">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" name="first_name" class="form-control"value="{{ old('first_name', $user->first_name) }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" id="last_name" name="last_name" class="form-control" value="{{ old('last_name', $user->last_name) }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', $user->email) }}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="role">Role</label>
                        <input type="text" id="role" name="role" class="form-control" value="{{ old('email', $user->role->name) }}" readonly>
                    </div>
                    <a href="{{route('users.index')}}" class="btn btn-md btn-primary pull-left">CANCEL</a>
                    <button type="submit" class="btn btn-md btn-danger pull-right">DELETE</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
