@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-6 col-lg-6 col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">User Profile</div>
            <div class="panel-body">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" class="form" action="{{ route('profile') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            @if($user->photo)
                            <img src="{{asset('storage/'.$user->photo->download_path)}}" class="img-responsive" alt="Photo Profile" accept="image/x-png,image/jpeg" >
                            @else
                            <img src="{{asset('img/default-profile.jpg')}}" class="img-responsive" alt="Photo Profile" accept="image/x-png,image/jpeg" >
                            @endif
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="form-group">
                                <label for="first_name">Photo Profile</label>
                                <input type="file" id="profile" name="profile" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" name="first_name" class="form-control" value="{{ old('first_name', $user->first_name) }}">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" id="last_name" name="last_name" class="form-control" value="{{ old('last_name', $user->last_name) }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" class="form-control" value="{{ old('email', $user->email) }}">
                    </div>
                    <div class="form-group">
                        <label for="biography">Biography</label>
                        <textarea id="biography" name="biography" class="form-control">{{old('biography', $user->biography)}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-md btn-primary pull-right">SAVE</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
