<div id="oc-images" class="owl-carousel image-carousel carousel-widget" data-margin="30" data-nav="false" data-items-xxs="1" data-items-xs="2" data-items-sm="4" data-items-lg="6">
    @foreach ($items as $item)
    <div class="oc-item">
        <div class="ipost clearfix">
            <div class="entry-image">
                @if($item->cover)
                    <a href="#"><img class="image_fade" src="{{asset('storage/'.$item->cover->download_path)}}" alt="Image"></a>
                @else
                    <a href="#"><img class="image_fade" src="{{asset('img/default-cover.jpg')}}" alt="Image"></a>
                @endif
            </div>
            <div class="entry-title">
                <h4><a href="{{route('book_details', ['id' => $item->id])}}">{{$item->title}}</a></h4>
            </div>
            <ul class="entry-meta clearfix">
                <li><i class="icon-calendar3"></i> {{$item->document_date}}</li>
            </ul>
        </div>
    </div>
    @endforeach
    <div class="oc-item">
        <div class="ipost clearfix">
            <div class="entry-image">
                <a href="#"><img class="image_fade" src="{{asset('img/default-cover.jpg')}}" alt="Image"></a>
            </div>
            <div class="entry-title">
                <h4><a href="/search">See more...</a></h4>
            </div>
            <ul class="entry-meta clearfix">
                {{-- <!--<li><i class="icon-calendar3"></i> {{$item->document_date}}</li>--> --}}
            </ul>
        </div>
    </div>
    {{-- <!--<div class="oc-item">-->
        <!--<div class="ipost clearfix">-->
            <!--<div class="entry-image">-->
                <!--<a href="#"><img class="image_fade" src="images/magazine/thumb/1.jpg" alt="Image"></a>-->
            <!--</div>-->
            <!--<div class="entry-title">-->
                <!--<h4><a href="blog-single.html">A Baseball Team Blew Up A Bunch Of Justin Bieber And Miley Cyrus Merch</a></h4>-->
            <!--</div>-->
            <!--<ul class="entry-meta clearfix">-->
                <!--<li><i class="icon-calendar3"></i> 13th Jun 2014</li>-->
                <!--<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 53</a></li>-->
            <!--</ul>-->
        <!--</div>-->
    <!--</div>--> --}}
</div>
