<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="dim4S" />


	<!-- Stylesheets
	============================================= -->
    <link rel="shortcut icon" href="images/new_ctiff_favicon.ico" type="image/x-icon">
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('frontend-theme/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('frontend-theme/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('frontend-theme/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('frontend-theme/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('frontend-theme/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('frontend-theme/css/magnific-popup.css')}}" type="text/css" />

	<link rel="stylesheet" href="{{asset('frontend-theme/css/responsive.css')}}" type="text/css" />
    <link rel="stylesheet" href="{{asset('frontend-theme/css/custom.css')}}" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Official Documents System - CTI-CFF | Coral Triangle Initiative on Coral Reefs Fisheries and Food Security</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Header
		============================================= -->
		@include('frontend_layouts/_header')

		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Document details</h1>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Categories</a></li>
					<li class="active">Book Details</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Post Content
					============================================= -->
					<div class="postcontent nobottommargin col_last clearfix">

						<div class="single-post nobottommargin">

							<!-- Single Post
							============================================= -->
							<div class="entry clearfix">

								<!-- Entry Title
								============================================= -->
								<div class="entry-title">
									<h2>{{$item->title}}</h2>
								</div><!-- .entry-title end -->

								<!-- Entry Meta
								============================================= -->
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> {{$item->document_date}}</li>
									<!--<li><a href="#"><i class="icon-user"></i> admin</a></li>-->
									<!--<li><i class="icon-folder-open"></i> <a href="#">General</a>, <a href="#">Media</a></li>-->
									<!--<li><a href="#"><i class="icon-comments"></i> 43 Comments</a></li>-->
									<!--<li><a href="#"><i class="icon-camera-retro"></i></a></li>-->
								</ul><!-- .entry-meta end -->

								<!-- Entry Content
								============================================= -->
								<div class="entry-content notopmargin">

									<!-- Entry Image
									============================================= -->
									<div class="entry-image alignleft">
                                        @if($item->cover)
                                        <a href="{{asset('storage/'.$item->cover->download_path)}}" data-lightbox="image"><img class="image_fade" src="{{asset('storage/'.$item->cover->download_path)}}" alt="Standard Post with Image"></a>
                                        @else
                                        <a href="/img/default-cover.jpg" data-lightbox="image"><img class="image_fade" src="{{asset('img/default-cover.jpg')}}" alt="Standard Post with Image"></a>
                                        @endif
									</div><!-- .entry-image end -->

									<p>{!!$item->summary!!}</p>

									<br>
                                    <ul class="list-unstyled">
                                        @foreach (extract_file_name($item->files) as $filename)
                                        <li>
                                            {{$filename}}
                                            <a href="/storage/{{find_file_by_name($item->files, $filename)->download_path}}" target="_blank">download</a>
                                            <br>
                                            </i>{{find_file_by_name($item->files, $filename)->description}}</i>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <p>
                                    Category: <strong>{{$item->item_type ? $item->item_type->name : '-' }}</strong>
                                    <br />
                                    Subject: <strong>{{ $item->subject ? $item->subject->name : '-'}}</strong>
                                    </p>


									<!-- Post Single - Content End -->

									<!-- Tag Cloud
									============================================= -->
									<div class="tagcloud clearfix bottommargin">
                                        @foreach($item->countries as $country)
                                        <a href="#">{{$country->name}}</a>
                                        @endforeach
									</div><!-- .tagcloud end -->

									<div class="tagcloud clearfix bottommargin">
                                        @foreach($item->tags as $tag)
                                        <a href="#">{{$tag->name}}</a>
                                        @endforeach
									</div><!-- .tagcloud end -->

									<div class="clear"></div>


								</div>
							</div><!-- .entry end -->

							<div class="panel panel-default">
								<div class="panel-heading">
                                    <h3 class="panel-title">Authors</h3>
								</div>
								<div class="panel-body">
                                    <ul class="list" style="margin-left: 15px;">
                                        @foreach($item->authors as $author)
                                        <li>{{ $author->name }}</li>
                                        @endforeach
                                    </ul>
								</div>
							</div><!-- Post Single - Author End -->

							<div class="panel panel-default">
								<div class="panel-heading">
                                    <h3 class="panel-title">Posted by <span><a href="#">{{$item->uploaded_by->full_name()}}</a></span></h3>
								</div>
								<div class="panel-body">
									<div class="author-image">
										<img src="{{asset('img/default-profile.jpg')}}" alt="" class="img-circle">
									</div>
                                    {{$item->uploaded_by->biography}}
								</div>
							</div><!-- Post Single - Author End -->

							<div class="line"></div>

							<h4>Related Document Item:</h4>

							<div class="related-posts clearfix">



								@foreach ($related_items as $k => $item)
								<div class="col_half nobottommargin {{$k % 2 > 0 ? 'col_last' : ''}}">

									<div class="mpost clearfix">
										<div class="entry-image">
                                            @if($item->cover)
                                            <a href="{{asset('storage/'.$item->cover->download_path)}}" data-lightbox="image"><img class="image_fade" src="{{asset('storage/'.$item->cover->download_path)}}" alt="Standard Post with Image"></a>
                                            @else
                                            <a href="/img/default-cover.jpg" data-lightbox="image"><img class="image_fade" src="{{asset('img/default-cover.jpg')}}" alt="Standard Post with Image"></a>
                                            @endif
										</div>
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="{{route('book_details', ['id' => $item->id])}}">{{$item->title}}</a></h4>
											</div>
											<ul class="entry-meta clearfix">
												<li><i class="icon-calendar3"></i> {{$item->document_date}}</li>
											</ul>
											<div class="entry-content">{{$item->preview}}</div>
										</div>
									</div>
								</div>
								@endforeach

							</div>



						</div>

					</div><!-- .postcontent end -->

					<!-- Sidebar
					============================================= -->
					<div class="sidebar nobottommargin clearfix">
						<div class="sidebar-widgets-wrap">

							<div class="widget clearfix">

								<div class="tabs nobottommargin clearfix" id="sidebar-tabs">

									<ul class="tab-nav clearfix">
										<!--<li><a href="#tabs-1">Popular</a></li>-->
										<li><a href="#tabs-1">Recent</a></li>

									</ul>

									<div class="tab-container">

										<div class="tab-content clearfix" id="tabs-2">
											<div id="recent-post-list-sidebar">

												@foreach ($items as $item)
													<div class="spost clearfix">
														<div class="entry-image">
                                                            @if($item->cover)
                                                            <a href="{{asset('storage/'.$item->cover->download_path)}}" data-lightbox="image"><img class="image_fade" src="{{asset('storage/'.$item->cover->download_path)}}" alt="Standard Post with Image"></a>
                                                            @else
                                                            <a href="/img/default-cover.jpg" data-lightbox="image"><img class="image_fade" src="{{asset('img/default-cover.jpg')}}" alt="Standard Post with Image"></a>
                                                            @endif
														</div>
														<div class="entry-c">
															<div class="entry-title">
																<h4><a href="{{route('book_details', ['id' => $item->id])}}">{{$item->title}}</a></h4>
															</div>
															<ul class="entry-meta">
																<li>{{$item->document_date}}</li>
															</ul>
														</div>
													</div>
												@endforeach

											</div>
										</div>


									</div>

								</div>

							</div>

						</div>

					</div><!-- .sidebar end -->

				</div>

			</div>

		</section><!-- #content end -->

		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">


			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2017 All Rights Reserved by <br>Coral Triangle Initiative on Coral Reefs Fisheries and Food Security.<br>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="https://web.facebook.com/CTICFF/" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="https://twitter.com/CTICFF" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
								<i class="icon-twitter"></i>
							</a>

							<a href="https://instagram.com/CTICFF" class="social-icon si-small si-borderless si-instagram">
								<i class="icon-instagram"></i>
								<i class="icon-instagram"></i>
							</a>

							<a href="https://www.youtube.com/user/coraltrianglevideo/videos" class="social-icon si-small si-borderless si-youtube">
								<i class="icon-youtube"></i>
								<i class="icon-youtube"></i>
							</a>

							<a href="http://coraltriangleinitiative.org" class="social-icon si-small si-borderless si-wikipedia">
								<i class="icon-wikipedia"></i>
								<i class="icon-wikipedia"></i>
							</a>
						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> info@cticff.org <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 4317 242 026 <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 4317 242 027
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="{{asset('frontend-theme/js/jquery.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend-theme/js/plugins.js')}}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="{{asset('frontend-theme/js/functions.js')}}"></script>


</body>
</html>
