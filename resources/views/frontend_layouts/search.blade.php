<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="dim4S" />


	<!-- Stylesheets
		============================================= -->
		<link rel="shortcut icon" href="images/new_ctiff_favicon.ico" type="image/x-icon">
		<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/css/bootstrap.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/style.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/css/dark.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/css/font-icons.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/css/animate.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/css/magnific-popup.css')}}" type="text/css" />

		<link rel="stylesheet" href="{{asset('frontend-theme/css/responsive.css')}}" type="text/css" />
		<link rel="stylesheet" href="{{asset('frontend-theme/css/custom.css')}}" type="text/css" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
		============================================= -->
		<title>Official Documents System - CTI-CFF | Coral Triangle Initiative on Coral Reefs Fisheries and Food Security</title>

	</head>

	<body class="stretched">

	<!-- Document Wrapper

		============================================= -->
		<div id="wrapper" class="clearfix">
			@include('frontend_layouts/_header')

		<!-- Page Title
			============================================= -->
			<section id="page-title">

				<div class="container clearfix">
					<h1>Search</h1>
					@include('frontend_layouts/_search_form')
				</div>

			</section><!-- #page-title end -->

			<!-- Content
				============================================= -->
				<section id="content">

					<div class="content-wrap">

						<div class="container clearfix">

					<!-- Posts
						============================================= -->
                        @if ($search_result->count() > 0)
						<div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
							@foreach ( $search_result as $item)
							<div class="entry clearfix">
								<div class="entry-image">
									@if($item->cover)
									<a href="{{asset('storage/'.$item->cover->download_path)}}" data-lightbox="image"><img class="image_fade" src="{{asset('storage/'.$item->cover->download_path)}}" alt="Standard Post with Image"></a>
									@else
										<a href="/img/default-cover.jpg" data-lightbox="image"><img class="image_fade" src="{{asset('img/default-cover.jpg')}}" alt="Standard Post with Image"></a>
									@endif
								</div>
								<div class="entry-title">
									<h2><a href="{{route('book_details', ['id' => $item->id])}}" target="_blank">{{ $item->title }}</a></h2>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> {{ $item->document_date }}</li>
									<li><a href="{{route('book_details', ['id' => $item->id])}}#files" target="_blank"><i class="icon-file"></i> {{$item->files->count()}}</a></li>
								</ul>
								<div class="entry-content">
									<p>{{$item->preview}}</p>
									<a href="{{route('book_details', ['id' => $item->id])}}"  target="_blank" class="more-link">Details...</a>
								</div>
							</div>
							@endforeach
						</div><!-- #posts end -->
                        @else
                        <h3>No result found</h3>
                        @endif

					<!-- Pagination
						============================================= -->
                        @if ($search_result->count() > 0)
                        <ul class="pagination nobottommargin">
                            {{$search_result->fragment('posts')->links()}}
                        </ul>
                        @endif
					<!-- .pager end -->

					</div>

				</div>

			</section><!-- #content end -->

			<!-- Footer
			============================================= -->
			<footer id="footer" class="dark">


				<div id="copyrights">

					<div class="container clearfix">

						<div class="col_half">
							Copyrights &copy; 2017 All Rights Reserved by <br>Coral Triangle Initiative on Coral Reefs Fisheries and Food Security.<br>
						</div>

						<div class="col_half col_last tright">
							<div class="fright clearfix">
								<a href="https://web.facebook.com/CTICFF/" class="social-icon si-small si-borderless si-facebook">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>

								<a href="https://twitter.com/CTICFF" class="social-icon si-small si-borderless si-twitter">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>

								<a href="https://instagram.com/CTICFF" class="social-icon si-small si-borderless si-instagram">
									<i class="icon-instagram"></i>
									<i class="icon-instagram"></i>
								</a>

								<a href="https://www.youtube.com/user/coraltrianglevideo/videos" class="social-icon si-small si-borderless si-youtube">
									<i class="icon-youtube"></i>
									<i class="icon-youtube"></i>
								</a>
								
								<a href="http://coraltriangleinitiative.org" class="social-icon si-small si-borderless si-wikipedia">
									<i class="icon-wikipedia"></i>
									<i class="icon-wikipedia"></i>
								</a>
							</div>

							<div class="clear"></div>

							<i class="icon-envelope2"></i> info@cticff.org <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 4317 242 026 <span class="middot">&middot;</span> <i class="icon-headphones"></i> +62 4317 242 027 
						</div>

					</div>

				</div><!-- #copyrights end -->

			</footer><!-- #footer end -->

		</div><!-- #wrapper end -->

	<!-- Go To Top
		============================================= -->
		<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
		============================================= -->
		<script type="text/javascript" src="{{asset('frontend-theme/js/jquery.js')}}"></script>
		<script type="text/javascript" src="{{asset('frontend-theme/js/plugins.js')}}"></script>

	<!-- Footer Scripts
		============================================= -->
		<script type="text/javascript" src="{{asset('frontend-theme/js/functions.js')}}"></script>


	</body>
</html>
