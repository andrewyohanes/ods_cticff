<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-9 col-sm-10">
            <span class="txt-color-darkgrey">CTI CFF ODS <span class="hidden-xs"> - an Official Documents System platform by CTI CFF</span> &copy; {{ date('Y') }}</span>
        </div>
        <div class="col-xs-2 col-sm-2">
          <p>v 1.12</p>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->
