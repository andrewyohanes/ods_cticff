<!DOCTYPE html>
<html lang="en-us">
    <head>
        @include('layouts/head')
        @yield('customstyle')
    </head>

    <body class="smart-style-0">

        @include('layouts/header')
        @include('layouts/navigation')

        <!-- MAIN PANEL -->
        <div id="main" role="main">

            <!-- RIBBON -->
            <div id="ribbon">

                <!-- breadcrumb -->
                <ol class="breadcrumb">
                    <li>Home</li><li>Dashboard</li><li>Social Wall</li>
                </ol>
                <!-- end breadcrumb -->

                <!-- You can also add more buttons to the
                    ribbon for further usability

                    Example below:

                    <span class="ribbon-button-alignment pull-right">
                    <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
                    <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
                    <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
                    </span> -->

            </div>
            <!-- END RIBBON -->



            <!-- MAIN CONTENT -->
            <div id="content">

                <!-- row -->
                <div class="row">

                    <!-- col -->
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                        <h1 class="page-title txt-color-blueDark">
                            <i class="fa-fw fa fa-home"></i> Dashboard <span> Social Wall </span>
                        </h1>
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->

                @include('layouts/message')
                @yield('content')

                <!--
                    The ID "widget-grid" will start to initialize all widgets below
                    You do not need to use widgets if you dont want to. Simply remove
                    the <section></section> and you can use wells or panels instead
                -->


            </div>
            <!-- END MAIN CONTENT -->

        </div>
        <!-- END MAIN PANEL -->


        @include('layouts/footer')
        @include('layouts/javascript')
        @yield('customjs')
    </body>
</html>
