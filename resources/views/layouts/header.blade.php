<!-- #HEADER -->
<header id="header">
    <div id="logo-group">
        <!-- PLACE YOUR LOGO HERE -->
        <a href="{{ route('home') }}">
            {{-- <span id="logo"> <img src="{{asset('storage/'. Auth::user()->photo->download_path)}}" alt="SmartAdmin"> </span> --}}
            <span id="logo"> <img src="{{asset('theme/img/logo.png')}}" alt="SmartAdmin"> </span>
        </a>
        <!-- END LOGO PLACEHOLDER -->

        <!-- END AJAX-DROPDOWN -->
    </div>

    <div class="btn-header pull-right">
      <span>
        <div class="badges">
          0
        </div>
        <a href="javascript:void(0)" id="notification" alt="Notifications" title="Notifications"><i class="fa fa-bell fa-lg"></i></a>
        <div class="notification-box">
        <div class="notification-head">
          <i class="glyphicon glyphicon-bell"></i>
          Notifications
        </div>
        <div class="notification-content">
          <div class="notification-container">
            <h4>No notification</h4>
          </div>
          <div class="notification-footer">
            <a href="{{ route('notifications') }}">See All</a>
          </div>
        </div>
      </div>
      </span>
    </div>
    <div class="btn-header pull-right">
      <span>
        <a href="{{ route('trash') }}" title="Trash" id="trash"><i class="fa fa-trash-o fa-lg"></i></a>
        <div class="trash-dialog">
          <ul>
            <li><a href="{{ route('trash') }}">Open</a></li>
            <li><a href="javascript:void(0)" class="empty-trash">Empty Trash</a></li>
          </ul>
        </div>
      </span>
    </div>


    <!-- #TOGGLE LAYOUT BUTTONS -->
    <!-- pulled right: nav area -->

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span><a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a></span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list padding-5">
            <li class="">
                <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                  <div class="online">
                    <img src="{{ (Auth::user()->photo == null) ? asset('img/default-profile.jpg') : asset('storage/'. Auth::user()->photo->download_path)}}" alt="John Doe" class="" />
                  </div>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="{{route('setting')}}" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{route('profile')}}" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}"
                        class="padding-10 padding-top-0 padding-bottom-0"
                        onclick="event.preventDefault();
                        $('#logout-form').submit();">
                            <i class="fa fa-sign-out fa-lg"></i> Logout </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>

    </div>
    <!-- end pulled right: nav area -->
<script type="text/html" id="template">
  <div data-class="class" class="notification-list">
    <div class="img">
      <img data-src="photo" alt="Profile">
    </div>
    <a data-href="href" target="_blank" class="title-min visible" data-id="id" data-content="title_min"></a>
    <a data-href="href" target="_blank" class="title-ori" data-id="id" data-content="title"></a>
    <!-- <span>5 minutes ago</span> -->
    <span data-content="time"></span>
    <button class="notif-button"><i class="glyphicon glyphicon-chevron-down"></i></button>
  </div>
</script>
</header>
<!-- END HEADER -->
<div class="loading">
  <div class="loading-icon">
    <i class="fa fa-spinner fa-fw fa-spin"></i>
  </div>
  <div class="loading-msg">
    Loading...
  </div>
</div>
