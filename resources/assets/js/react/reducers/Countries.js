import { FETCH_COUNTRIES } from '../actions'

const countries = (state=[], action) => {
    switch(action.type) {
        case FETCH_COUNTRIES:
            return action.countries
        default:
            return state
    }
}

export { countries }
