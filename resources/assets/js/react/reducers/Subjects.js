import { FETCH_SUBJECTS } from '../actions'


const subjects = (state=[], action) => {
    switch(action.type) {
        case FETCH_SUBJECTS:
            return action.subjects
        default:
            return state
    }
}

export { subjects }
