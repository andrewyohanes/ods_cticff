import { FETCH_TAGS } from '../actions'


const tags = (state=[], action) => {
    switch(action.type) {
        case FETCH_TAGS:
            return action.tags
        default:
            return state
    }
}

export { tags }
