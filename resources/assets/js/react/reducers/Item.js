import { GET_ITEM, FETCH_ITEMS } from '../actions'

const items = (state=[], action) => {
    switch(action.type) {
        case FETCH_ITEMS:
            return action.items
        default:
            return state
    }
}

const item = (state={}, action) => {
    switch(action.type) {
        case GET_ITEM:
            return action.item
        default:
            return state
    }
}


export { items, item }
