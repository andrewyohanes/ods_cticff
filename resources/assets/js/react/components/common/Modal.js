import React from 'react';
import Modal from 'react-bootstrap-modal';
import 'react-bootstrap-modal/lib/css/rbm-patch.css';


export default class ModalEl extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            disabled: false
        }

        this.toggleDisabled = this.toggleDisabled.bind(this)
        this.onClick = this.onClick.bind(this)
        this.onHide = this.onHide.bind(this)
    }

    toggleDisabled() {
        this.setState((prevState, props) => {
            return { disabled: !prevState.disabled }
        })
    }

    onClick(e) {
        e.preventDefault()
        this.toggleDisabled()
        this.props.deleteItem(this.props.delete.item)
    }

    onHide() {
        console.log('hiding')
        if(this.state.disabled) {
            this.toggleDisabled()
        }

        this.props.closeModal()
    }

    render() {
        return (
            <div>
                <Modal
                    show={this.props.modal.open}
                    onHide={this.onHide}
                    onExited={this.onHide}
                    backdrop="static"
                    keyboard={false}
                    aria-labelledby="ModalHeader"
                >
                    <Modal.Header>
                        <Modal.Title id='ModalHeader'>Delete Item ID: {this.props.delete.item.id}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <h3>Delete Item with title: <strong>{this.props.delete.item.title}</strong>?</h3>
                    </Modal.Body>
                    <Modal.Footer>

                        {this.state.disabled ?
                                <button type="button" className="btn btn-default" disabled={true}>
                                    <i className="fa fa-gear fa-spin"></i> Submitting...
                                </button>
                                :
                                <div>
                                    <Modal.Dismiss className='btn btn-default'>Cancel</Modal.Dismiss>
                                    <button className='btn btn-primary' onClick={this.onClick}>
                                        Delete!
                                    </button>
                                </div>
                        }

                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}
