import React from 'react';
import moment from 'moment';
import { withFormsy  } from 'formsy-react';
import DatePicker from 'react-datepicker';
import CustomDatePicker from './CustomDatePicker';
import 'react-datepicker/dist/react-datepicker.css';
import '../../../../sass/datepicker.scss';


class InputDatePicker extends React.Component {
    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this)
    }

    onChange(date) {
        this.props.setValue(date.format("YYYY-MM-DD"))
        this.props.onChange(date)
    }

    componentDidMount() {
        this.props.setValue(this.props.date.format("YYYY-MM-DD"))
    }

    render() {
        return(
            <DatePicker customInput={
                <CustomDatePicker {...this.props} disabled={this.props.isFormDisabled()} />} disabled={this.props.isFormDisabled()} selected={this.props.date} onChange={this.onChange} dateFormat="DD MMMM YYYY" />
        )
    }
}

export default withFormsy(InputDatePicker)
