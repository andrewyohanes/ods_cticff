import React from 'react';

export default class Panel extends React.Component {
    render() {
        return(
            <div className="row">
                <div className={ this.props.size ? this.props.size : "col-md-8" }>
                    <div className="panel panel-default">
                        <div className="panel-heading">{this.props.title}</div>

                        <div className="panel-body">
                            {this.props.children}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
