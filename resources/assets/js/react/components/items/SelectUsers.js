import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { propTypes, withFormsy } from 'formsy-react';

class SelectElement extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            value: []
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(value) {
        console.log(value)
        this.props.setUsers(value);
        this.props.setValue(value);
    }

    componentDidMount() {
        this.props.setValue(this.props.value)
    }

    render() {
        return (
            <div className="form-group">
                <label htmlFor={this.props.name}>
                    {this.props.label}{this.props.required ? "*" : ""}
                </label>
                <Select
                    multi
                    removeSelected={true}
                    name={this.props.name}
                    value={this.props.value}
                    onChange={this.handleChange}
                    options={this.props.options}
                    disabled={this.props.disabled}
                />
            </div>
        );
    }
}


SelectElement.propTypes = {
    ...propTypes
};
export default withFormsy(SelectElement);
