import React from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import { ItemsReq } from '../../models';
import Panel from '../common/Panel';
import Modal from '../common/Modal';
import ReactTable from "react-table";
import "react-table/react-table.css";
const baseURL = document.head.querySelector('meta[name="base_url"]').content;


class Table extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            modal: {
                open: false
            },
            delete: {
                item: {}
            }
        }

        this.closeModal = this.closeModal.bind(this)
        this.openModal = this.openModal.bind(this)
        this.deleteItem = this.deleteItem.bind(this)
    }

    closeModal() {
        this.setState((prevState, props) => {
            return update(prevState, {
                modal: {
                    $set: { open: false }
                }
            })
        })
    }

    deleteItem(item) {
        ItemsReq.delete(`/${item.id}`)
            .then((res) => {
                console.log(res)
                this.closeModal()
                this.props.fetchItems()
            })
            .catch((err) => {
                console.log(err)
                this.closeModal()
            })
    }

    setDeleteItem(item) {
        this.setState((prevState, props) => {
            return update(prevState, {
                delete: {
                    item: {
                        $set: item
                    }
                }
            })
        })
    }

    openModal() {
        this.setState((prevState, props) => {
            return update(prevState, {
                modal: {
                    $set: { open: true }
                }
            })
        })
    }

    componentDidMount() {
        this.props.fetchItems()
    }

    render() {
        const data = this.props.items.map((item, idx) => {
            return {
                no: idx+1,
                title: item.title,
                category: item.item_type ? item.item_type.name : '',
                subject: item.subject ? item.subject.name : '',
                document_date: item.document_date,
                files: item.files.length,
                published: item.published == 1 ? 'YES' : 'NO',
                created_at: item.created_at,
                updated_at: item.updated_at,
                actions: item.id
            }
        });

        return(
            <div>
                <Panel title="List Items" size="col-md-12 col-lg-12 col-sm-12">
                    <ReactTable
                        data={data}
                        columns={[
                            {
                                columns: [
                                    {
                                        Header: "No",
                                        accessor: "no",
                                        width: 50,
                                    },
                                    {
                                        Header: "Title",
                                        accessor: "title",
                                        width: 300,
                                    },
                                    {
                                        Header: "Category",
                                        accessor: "category",
                                        width: 150,
                                    },
                                    {
                                        Header: "Subject",
                                        accessor: "subject",
                                        width: 150,
                                    },
                                    {
                                        Header: "Document Date",
                                        accessor: "document_date",
                                        width: 120,
                                    },
                                    {
                                        Header: "Published",
                                        accessor: "published",
                                        width: 75,
                                    },
                                    {
                                        Header: "Created at",
                                        accessor: "created_at",
                                        width: 120
                                    },
                                    {
                                        Header: "Updated at",
                                        accessor: "updated_at",
                                        width: 120,
                                    },
                                    {
                                        Header: 'Actions',
                                        accessor: 'actions',
                                        width: 75,
                                        Cell: row => (
                                            <span>
                                                <a href={`${baseURL}/admin/items/${row.value}`}>
                                                    <i className="fa fa-eye" aria-hidden="true"></i>
                                                </a>
                                                {' '}
                                                <a href={`${baseURL}/admin/items/${row.value}/edit`}>
                                                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                                {' '}
                                                <a className="btn-delete" href="#" data-item-id="{{$item->id}}" onClick={(e) => {
                                                    e.preventDefault()
                                                    this.setDeleteItem(this.props.items.filter(i => i.id == row.value)[0])
                                                    this.openModal()
                                                }}>
                                                    <i className="fa fa-trash-o" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        )
                                    }
                                ]
                            }
                        ]}
                        defaultPageSize={30}
                        className="-striped -highlight"
                    />
                </Panel>
                <Modal closeModal={this.closeModal} openModal={this.openModal} modal={this.state.modal} deleteItem={this.deleteItem} delete={this.state.delete} />
            </div>
        )
    }
}

Table.propTypes = {
    items: PropTypes.array.isRequired,
    fetchItems: PropTypes.func.isRequired
}

export default Table;
