import React from 'react';
import PropTypes from 'prop-types';
import update from 'immutability-helper';
import Formsy from 'formsy-react';
import { ItemsReq, ItemCoverReq, visibility } from '../../models';
import Input from '../common/Input';
import TextArea from '../common/TextArea';
import InputDatePicker from '../common/InputDatePicker';
import Select from '../common/Select';
import uploader from './Uploader';
import SelectCountries from './SelectCountries';
import SelectAuthors from './SelectAuthors';
import SelectTags from './SelectTags';
import SelectUsers from './SelectUsers';
import InputImagePreview from './ImagePreview';

import Gallery from 'react-fine-uploader'
import 'react-fine-uploader/gallery/gallery.css'

import { addValidationRule  } from 'formsy-react';

addValidationRule('isFileSize', function (values, value, sizeInByte) {
    return (value && value.size <= Number(sizeInByte))
});


class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            canSubmit: false,
            canUpload: false,
            disabled: false,
            selectedCountries: [],
            selectedAuthors: [],
            selectedUsers: [],
            selectedTags: [],
            docDate: moment(),
            itemId: 0,
            cover: {
                file: '',
                imagePreviewUrl: ''
            }
        }

        this.submit = this.submit.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onValid = this.onValid.bind(this);
        this.onInvalid = this.onInvalid.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onChangeDocDate = this.onChangeDocDate.bind(this);
        this.setCountries = this.setCountries.bind(this);
        this.clearCountries = this.clearCountries.bind(this);
        this.setAuthors = this.setAuthors.bind(this);
        this.clearAuthors = this.clearAuthors.bind(this);
        this.setUsers = this.setUsers.bind(this);
        this.clearUsers = this.clearUsers.bind(this);
        this.setTags = this.setTags.bind(this);
        this.clearTags = this.clearTags.bind(this);
        this.setCover = this.setCover.bind(this);
        this.clearCover = this.clearCover.bind(this);
    }

    setCover(cover) {
        console.log(cover.file.name)
        this.setState((prevState, props) => {
            return(update(prevState, {
                cover: {
                    $set: { ...cover }
                }
            }))
        })
    }

    clearCover() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                cover: {
                    $set: {
                        file: '',
                        imagePreviewUrl: ''
                    }
                }
            }))
        })
    }

    onChangeDocDate(date) {
        this.setState((prevState, props) => {
            return(update(prevState, { docDate: { $set: date } }))
        })
    }

    clearTags() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedTags: {
                    $set: []
                }
            }))
        })
    }

    setTags(tags) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedTags: {
                    $set: tags
                }
            }))
        })
    }

    clearAuthors() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedAuthors: {
                    $set: []
                }
            }))
        })
    }

    setAuthors(authors) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedAuthors: {
                    $set: authors
                }
            }))
        })
    }

    setUsers(users) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedUsers: {
                    $set: users
                }
            }))
        })
    }

    clearUsers() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedUsers: {
                    $set: []
                }
            }))
        })
    }

    clearCountries() {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedCountries: {
                    $set: []
                }
            }))
        })
    }

    setCountries(countries) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                selectedCountries: {
                    $set: countries
                }
            }))
        })
    }

    onSubmit(data) {
        console.log(data)
        this.setState({disabled: true})
        ItemsReq.post('', data, {'content-type': 'multipart/form-data'})
            .then((res) => {
                console.log(res)
                // cover uploader
                let dataCover = new FormData()
                dataCover.append('cover', this.state.cover.file)
                ItemCoverReq(res.data.id).post("", dataCover)
                    .then((resCover) => {
                        console.log(resCover)
                    })
                    .catch((errCover) => {
                        console.log(errCover)
                    })
                // end cover uploader

                // file uploader
                uploader.methods.setParams({item_id: res.data.id})
                uploader.methods.uploadStoredFiles()
                this.refs.form.reset({
                    document_date: moment().format("YYYY-MM-DD")
                });
                // end of file uploader
                //
                this.setState({ 'docDate': moment() })
                this.clearCountries()
                this.clearAuthors()
                this.clearTags()
                this.clearUsers()

                notification({
                    title: 'Create Item',
                    text: 'Item Created!',
                    icon: 'fa fa-check-square-o',
                    type: 'success'
                })

                this.setState({disabled: false})
                this.props.fetchTags()
            })
            .catch((err) => {
                console.log(err)

                notification({
                    title: 'Create Item',
                    text: 'Error when creating item, please refresh this page if error still occur',
                    icon: 'fa fa-times-circle',
                    type: 'error'
                })

                this.setState({disabled: false})
            })
    }

    onValid() {
        this.setState({ canSubmit: true })
    }

    onInvalid() {
        this.setState({ canSubmit: false })
    }

    submit() {
    }

    componentDidMount() {
        this.props.fetchItemTypes()
        this.props.fetchCountries()
        this.props.fetchAuthors()
        this.props.fetchTags()
        this.props.fetchSubjects()
        this.props.fetchUsers()

        uploader.on('statusChange', (id, oldStatus, newStatus) => {
            this.setState((prevState, props) => {
                return update(prevState, {
                    canUpload: {
                        $set: uploader.methods.getUploads({ status: uploader.qq.status.SUBMITTED }).length > 0
                    }
                })
            })
        })
    }

    onChange(currentValues, isChanged) {
        //this.props.setNewConnection(this.refs.form.getModel())
    }

    mapInputs(inputs) {
        return {
            title: inputs.title,
            alternative_title: inputs.alternative_title || "",
            preview: inputs.preview || "",
            summary: inputs.summary || "",
            document_date: inputs.document_date,
            item_type_id: parseInt(inputs.item_type_id),
            published: parseInt(inputs.published) || 1,
            countries: inputs.countries || [],
            authors: inputs.authors || [],
            tags: inputs.tags || [],
            subject_id: parseInt(inputs.subject_id),
            visibility: inputs.visibility || 0,
            visibility_users: inputs.visibility_users || []
        };

    }

    visibilityUsersIsRequired() {
        return this.refs.form && parseInt(this.refs.form.getModel()['visibility']) > 2
    }

    render() {
        return (
            <Formsy ref="form" onSubmit={this.onSubmit} onValid={this.onValid} onInvalid={this.onInvalid} mapping={this.mapInputs} onChange={this.onChange} disabled={this.state.disabled}>
                <Input name="title" label="Title" required={true} />
                <Input name="alternative_title" label="Alternative Title" />
                <TextArea name="preview" label="Preview" />
                <TextArea name="summary" label="Summary" />
                <InputImagePreview name="cover" label="Cover Image" validations="isFileSize:2097152" validationError="Max file size is 2MB!" required={true} setCover={this.setCover} {...this.state.cover} clearCover={this.clearCover}/>
                <InputDatePicker name="document_date" label="Document date" date={this.state.docDate} onChange={this.onChangeDocDate} />
                <SelectCountries name="countries" label="Countries" options={this.props.countries.map((country,idx) => { return { value: country.id, label: country.name } })} setCountries={this.setCountries} value={this.state.selectedCountries}/>
                <SelectAuthors name="authors" label="Authors" options={this.props.authors.map((author,idx) => { return { value: author.id, label: author.name } })} setAuthors={this.setAuthors} value={this.state.selectedAuthors}/>
                <Select name="item_type_id" label="Category" options={this.props.itemTypes.map((itemType) => { return {value: itemType.id, name: itemType.name} })}  />
                <Select name="subject_id" label="Subject" options={this.props.subjects.map((subject) => { return {value: subject.id, name: subject.name} })}  />
                <SelectTags name="tags" label="Tags" options={this.props.tags.map((tags,idx) => { return { value: tags.id, label: tags.name } })} setTags={this.setTags} value={this.state.selectedTags}/>
                <Select name="published" label="Publish?" options={[{value: 1, name: 'Yes'}, {value:0, name: 'No'}]} required={true} />
                <hr />
                <Select name="visibility" label="Set Item Visibility" options={visibility} required={true} />
                <SelectUsers name="visibility_users" label="Visibility Users" options={this.props.users.filter(user => !user.current_user).map((user,idx) => { return { value: user.id, label: `${user.first_name} ${user.last_name}` } })} setUsers={this.setUsers} value={this.state.selectedUsers} required={this.visibilityUsersIsRequired()} disabled={!this.visibilityUsersIsRequired()}/>
                <hr />
                <Gallery uploader={ uploader } disabled={this.state.disabled} />
                <hr />
                {!this.state.disabled ?
                        <button type="submit" className="btn btn-success btn-lg" disabled={!(this.state.canSubmit && this.state.canUpload)}>Submit!</button>
                        :
                        <button type="button" className="btn btn-default btn-lg" disabled={true}>
                            <i className="fa fa-gear fa-spin"></i> Submitting...
                        </button>
                }

            </Formsy>
        );
    }
}

Form.propTypes = {
    itemTypes: PropTypes.array.isRequired,
    countries: PropTypes.array.isRequired,
    authors: PropTypes.array.isRequired,
    tags: PropTypes.array.isRequired,
    subjects: PropTypes.array.isRequired,
    fetchItemTypes: PropTypes.func.isRequired,
    fetchCountries: PropTypes.func.isRequired,
    fetchAuthors: PropTypes.func.isRequired,
    fetchTags: PropTypes.func.isRequired,
    fetchSubjects: PropTypes.func.isRequired
}

export default Form;
