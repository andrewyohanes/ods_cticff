import React from 'react';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { propTypes, withFormsy } from 'formsy-react';

class SelectElement extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            value: []
        }

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(value) {
        console.log(value)
        this.props.setTags(value);
        this.props.setValue(value);
    }

    componentDidMount() {
        this.props.setValue(this.props.value)
    }

    render() {
        return (
            <div className="form-group">
                <label htmlFor={this.props.name}>{this.props.label}</label>
                <Select.Creatable
                    multi
                    removeSelected={true}
                    name={this.props.name}
                    value={this.props.value}
                    onChange={this.handleChange}
                    options={this.props.options}
                />
            </div>
        );
    }
}


SelectElement.propTypes = {
    ...propTypes
};

export default withFormsy(SelectElement);
