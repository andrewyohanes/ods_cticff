const baseURL = document.head.querySelector('meta[name="base_url"]').content;
console.log(`From react ${baseURL}`);
export const ItemTypesReq = axios.create({
    baseURL: baseURL + '/api/categories',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const ItemsReq = axios.create({
    baseURL: baseURL + '/api/items',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const CountriesReq = axios.create({
    baseURL: baseURL + '/api/countries',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const ItemCoverReq = (itemId) => {
    const ItemCover = axios.create({
        baseURL: `${baseURL}/api/items/${itemId}/cover`,
        timeout: 3600000,
        headers: {'Content-Type': 'application/json'}
    });

    return ItemCover
}

export const AuthorsReq = axios.create({
    baseURL: baseURL + '/api/authors',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const TagsReq = axios.create({
    baseURL: baseURL + '/api/tags',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const SubjectsReq = axios.create({
    baseURL: baseURL + '/api/subjects',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const UsersReq = axios.create({
    baseURL: baseURL + '/api/users',
    timeout: 3600000,
    headers: {'Content-Type': 'application/json'}
});

export const visibility = [
    { value: 1, name: 'All can see' },
    { value: 2, name: 'Only registered users' },
    { value: 4, name: 'Registered users only' }
]
