import { TagsReq } from '../models'

const FETCH_TAGS = 'FETCH_TAGS'

const fetchTagsAsync = (tags) => {
    return {
        type: FETCH_TAGS,
        tags
    }
}

const fetchTags = () => {
    return (dispatch) => {
        TagsReq.get('/')
            .then(res => {
                dispatch(fetchTagsAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export { FETCH_TAGS, fetchTags }
