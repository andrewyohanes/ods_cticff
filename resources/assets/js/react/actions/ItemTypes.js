import { ItemTypesReq } from '../models'


const FETCH_ITEM_TYPES = 'FETCH_ITEM_TYPES'


const fetchItemTypesAsync = (itemTypes) => {
    return {
        type: FETCH_ITEM_TYPES,
        itemTypes
    }
}

const fetchItemTypes = () => {
    return (dispatch) => {
        ItemTypesReq.get('/')
            .then(res => {
                dispatch(fetchItemTypesAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export { FETCH_ITEM_TYPES, fetchItemTypes }
