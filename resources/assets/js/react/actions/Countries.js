import { CountriesReq } from '../models'

const FETCH_COUNTRIES = 'FETCH_COUNTRIES'

const fetchCountriesAsync = (countries) => {
    return {
        type: FETCH_COUNTRIES,
        countries
    }
}

const fetchCountries = () => {
    return (dispatch) => {
        CountriesReq.get('/')
            .then(res => {
                dispatch(fetchCountriesAsync(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export { FETCH_COUNTRIES, fetchCountries }
