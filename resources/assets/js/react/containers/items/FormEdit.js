import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchItemTypes, fetchCountries, fetchAuthors, fetchTags, fetchSubjects, fetchUsers } from '../../actions';
import { getItem  } from '../../actions';
import FormElement from '../../components/items/FormEdit.js';


const mapStateToProps = state => {
    return {
        authors: state.authors,
        countries: state.countries,
        itemTypes: state.itemTypes,
        item: state.item,
        tags: state.tags,
        subjects: state.subjects,
        users: state.users
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        fetchAuthors: fetchAuthors,
        fetchItemTypes: fetchItemTypes,
        fetchCountries: fetchCountries,
        fetchTags: fetchTags,
        getItem: getItem,
        fetchSubjects: fetchSubjects,
        fetchUsers
    }, dispatch)
}

const Form = connect(
    mapStateToProps,
    mapDispatchToProps
)(FormElement)

export default Form;
