import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware  } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducers from './reducers'
import CreateItem from './components/items/Create';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducers, devToolsEnhancer());

ReactDOM.render(
    <Provider store={store}>
        <CreateItem />
    </Provider>,
    document.getElementById('app-root'))
