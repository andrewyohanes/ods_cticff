import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class TextArea extends Component {
    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
        let targetKey = e.target.id
        let newVal = e.target.value

        this.props.onChangeValue(targetKey, newVal)
    }

    render() {
        return(
            <div className="form-group">
                <label htmlFor={this.props.identity}>
                    {this.props.label}
                </label>
                <textarea
                    id={this.props.identity}
                    name={this.props.identity}
                    className="form-control"
                    value={this.props.value}
                    onChange={this.onChange}
                >
                </textarea>
            </div>
        )
    }
}
