import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import update from 'immutability-helper';
import { Items, ItemTypes } from './models'
import moment from 'moment';
import Input from './Input';
import TextArea from './TextArea';
import Select from './Select';
import InputDatePicker from './InputDatePicker';


export default class ItemCreateForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            item: {
                title: "",
                altTitle: "",
                review: "",
                summary: "",
                docDate: "",
                itemType: "",
                items: []
            },
            itemType: {
                options: []
            },
            docDate: moment()
        }

        this.onChangeValue = this.onChangeValue.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.handleChangeDocDate = this.handleChangeDocDate.bind(this)
        this.transformItem = this.transformItem.bind(this)
    }

    onSubmit(e) {
        e.preventDefault()
        let item = this.transformItem(this.state.item)

        Items.post('/', {...item})
            .then((response) => {
                console.log(response)
            })
            .catch((error) => {
                console.log(error)
            })
    }

    transformItem(item) {
        return update(item, {
            $merge: {
                item_type_id: {
                    $set: item.itemType
                },
                document_date: {
                    $set: item.docDate
                },
                alternative_title: {
                    $set: item.altTitle
                }
            }
        })
    }

    handleChangeDocDate(date) {
        this.setState((prevState, props) => {
            return(update(prevState, { docDate: { $set: date } }))
        })

        this.onChangeValue('docDate', date.format("YYYY-MM-DD"));
    }

    componentDidMount() {
        ItemTypes.get("/")
            .then((response) => {
                this.setState((prevState, props) => {
                    return(update(prevState, {
                        itemType: {
                            options: {
                                $set: response.data
                            }
                        }
                    }))
                })
            })
            .catch((error) => {
                console.log(error);
            });
    }

    onChangeValue(targetKey, newVal) {
        this.setState((prevState, props) => {
            return(update(prevState, {
                item: {
                    [targetKey]: {
                        $set: newVal
                    }
                }
            }))
        })
    }

    render() {
        return (
            <form onSubmit={this.onSubmit}>
                <Input identity="title" label="Title" value={this.state.item.title} onChangeValue={this.onChangeValue} />

                <Input identity="altTitle" label="Alternative Title" value={this.state.item.altTitle} onChangeValue={this.onChangeValue} />

                <TextArea identity="review" label="Review" value={this.state.item.review} onChangeValue={this.onChangeValue} />

                <TextArea identity="summary" label="Summary" value={this.state.item.summary} onChangeValue={this.onChangeValue} />

                <Select identity="itemType" label="Item Type" value={this.state.item.itemType} onChangeValue={this.onChangeValue} options={this.state.itemType.options} />

                <InputDatePicker identity="docDate" label="Document date" date={this.state.docDate} onChange={this.handleChangeDocDate} />


                <button type="submit" className="btn btn-md btn-success" onClick={this.onSubmit}>Create!</button>
            </form>
        );
    }
}


if (document.getElementById('item-create-form')) {
    ReactDOM.render(<ItemCreateForm />, document.getElementById('item-create-form'));
}
