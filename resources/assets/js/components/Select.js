import React, { Component } from 'react';
import ReactDOM from 'react-dom';


export default class Select extends Component {
    constructor(props) {
        super(props)

        this.onChange = this.onChange.bind(this)
    }

    onChange(e) {
        let targetKey = e.target.id
        let newVal = e.target.value

        this.props.onChangeValue(targetKey, newVal)
    }
    render() {
        return(
            <div className="form-group">
                <label htmlFor="itemType">{this.props.label}</label>
                <select id="itemType" name="itemType" className="form-control" value={this.props.value} onChange={this.onChange}>
                    <option value="" default>Select {this.props.label}...</option>
                    {this.props.options.map((opt, i) => {
                        return(
                            <option key={i} value={opt.id}>{opt.name}</option>
                        )
                    })}
                </select>
            </div>
        )
    }
}
