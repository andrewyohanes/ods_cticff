<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function total_users()
    {
        return $this->users->count();
    }

    public function isAdmin()
    {
        return $this->name == "admin";
    }

    public function isRoot()
    {
        return $this->name == "root";
    }
}
