<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogItem extends Model
{
    //
    protected $fillable = ["user_id", "item_id", "submit_action"];

    public function log_user_notifications() {
        return $this->hasMany('App\LogUserNotification', 'log_item_id', 'id');
    }

    public function item() {
        return $this->belongsTo('App\Item', 'item_id', 'id')->withTrashed();
    }

}
