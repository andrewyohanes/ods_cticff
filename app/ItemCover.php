<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCover extends Model
{

    protected $fillable = [
        'path', 'filename', 'description', 'filetype', 'file_id', 'token', 'download_path', 'item_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id', 'id');
    }

    public function delete()
    {
        Storage::delete($this->path);
        return parent::delete();
    }
}
