<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visibility extends Model
{
    protected $fillable = [
        'type'
    ];

    public function item()
    {
        return $this->hasOne('App\Item', 'item_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'visibility_users');
    }
}
