<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'biography'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function items()
    {
        return $this->hasMany('App\Item', 'user_id', 'id');
    }

    public function full_name()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function is_role($role_name)
    {
        return $this->role->name == $role_name;
    }

    public function photo()
    {
        return $this->hasOne('App\UserPhoto', 'user_id', 'id');
    }

    public function isAdmin()
    {
        return $this->role->isAdmin();
    }

    public function isRoot()
    {
        return $this->role->isRoot();
    }

    public function delete()
    {
        $user_target = User::find(1);
        foreach($this->items as $item)
        {
           $item->uploaded_by()->associate($user_target);
           $item->save();
        }

        return parent::delete();
    }
}
