<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = [
        'code', 'name'
    ];

    public function items()
    {
        return $this->belongsToMany('App\Item', 'item_countries')->withTimestamps();
    }
}
