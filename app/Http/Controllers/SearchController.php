<?php

namespace App\Http\Controllers;

use Log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use App\Item;
use App\ItemType;
use App\ItemFiles;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('items.index', ['items' => Item::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $current_date = Carbon::now()->toDateString();
        return view('items.create', ['item_types' => ItemType::all(), 'current_date' => $current_date]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'alternative_title' => 'string|max:255',
            'preview' => 'string',
            'summary' => 'string',
            'document_date' => 'date',
            'item_type_id' => 'required|integer|exists:item_types,id',
            'item_files' => 'required',
            'published' => 'required'
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $item = Item::create([
            'title' => $request['title'],
            'alternative_title' => $request['alternative_title'],
            'preview' => $request['preview'],
            'summary' => $request['summary'],
            'document_date' => $request['document_date'],
            'published' => $request['published']
        ]);

        $item_type = ItemType::find($request['item_type_id']);

        $item->uploaded_by()->associate(Auth::user());
        $item->item_type()->associate($item_type);
        $item->save();

        $item_files_id = array_map('intval', explode(',', $request['item_files']));
        $item_files_list = ItemFiles::findMany($item_files_id);
        $item->files()->saveMany($item_files_list);

        return back()->with('alert-success', 'Item created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('items.view', ['item' => Item::find($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
