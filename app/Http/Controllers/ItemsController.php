<?php

namespace App\Http\Controllers;

use Log;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use App\Item;
use App\ItemType;
use App\ItemFiles;

class ItemsController extends Controller
{
    public function index()
    {
        $items = Item::with("item_type", "subject", "tags")->userId()->paginate(15);
        return view('items.index')->with("items", $items);
    }

    public function create()
    {
        return view('items.create');
    }

    public function show($id)
    {
        $item = Item::with("visibility.users")->findOrFail($id);
        if($item->visibility->type == 4) {
            if(!$this->is_user_allowed($item->visibility->users) && $item->user_id != auth()->user()->id) {
                return abort(404);
            }
        }
        return view('items.view_new', ['item' => $item]);
    }

    public function edit($id)
    {
        $item = Auth::user()->items()->findOrFail($id);
        return view('items.edit', ['item' => $item]);
    }

    public function current_user()
    {
        return Auth::user();
    }

    private function is_user_allowed($users) {
        return in_array(auth()->user()->id, array_map(function($user){
            // dd($user[0]);
            return $user[0]->id;
        }, (array)$users));
    }
}
