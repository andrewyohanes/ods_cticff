<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\ItemFiles;
use App\Models\ItemFile;
use App\Item;
use App\Http\Resources\ItemFiles as ItemFilesResource;

class ItemFilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ItemFilesResource(ItemFiles::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $path = $file->path();
        $ext = $file->extension();
        $filename = $request['resumableFilename'];
        $file_id = $request['resumableIdentifier'];
        $token = $request['tokenSession'];

        $file_path = $file->store('public/upload');
        $download_path = explode('public/', $file_path)[1];

        $item_file = ItemFiles::create([
            'path' => $file_path,
            'filename' => $filename,
            'file_id' => $file_id,
            'token' => $token,
            'description' => $request['description'],
            'filetype' => $ext,
            'download_path' => $download_path
        ]);

        $item_file->save();
        return response()->json($item_file);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item_file = ItemFiles::findOrFail($id);
        return new ItemFilesResource($item_file);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function get_items($total = 5, $offset=0)
    {
      $data = [];
      $data['item_total'] = ItemFiles::all()->count();
      // $item_file = DB::table('item_files')->join('items', 'item_files.item_id', '=', 'items.id')->limit($total)->offset($offset);
      // $user = DB::table('users')->join('user_photos', 'user_photos.user_id', '=', 'users.id');
      // return $user;

      $data['item'] = DB::table('items')
      ->where('user_id', Auth::user()->id)
      ->join('users', 'items.user_id', '=', 'users.id')
      ->join('item_files', 'item_files.item_id', '=', 'items.id')
      // ->join('user_photos', 'items.user_id', '=', 'user_photos.user_id')
      ->where('user_id', Auth::user()->id)
      ->limit($total)
      ->offset($offset)
      ->get();
      return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
