<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Author;

class AuthorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role', ['except' => ['index']]);
    }

    public function index()
    {
        return view('authors.index', ['authors' => Author::all()]);
    }

    public function create()
    {
        return view('authors.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $country = Author::create([
            'name' => $request['name'],
        ]);

        return back()->with('alert-success', 'New country created');

    }

    public function edit($id)
    {
        $author = Author::findOrFail($id);
        return view('authors.edit', ['author' => $author]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $author = Author::findOrFail($id);
        $author->name = $request['name'];
        $author->save();

        return back()->with('alert-success', 'Author data saved');
    }

    public function destroy($id)
    {
        $author = Author::findOrFail($id);
        $author->delete();
        return redirect()->route('authors.index');
    }

    public function delete($id)
    {
        $author = Author::findOrFail($id);
        return view('authors.delete', ['author' => $author]);
    }
}
