<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Role;
use App\UserPhoto;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('role', ['except' => ['index']]);
    }

    public function index()
    {
        return view('users.index', [
            'users' => User::all()
        ]);
    }

    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('users.view', ['user' => $user]);
    }

    public function create()
    {
        if(Auth::user()->is_role('contributor')) {
            return redirect()->route('dashboard');
        }

        return view('users.create', [
            'roles' => Role::all()
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|integer|exists:roles,id'
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $role = Role::find($request['role_id']);

        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'biography' => ''
        ]);

        $user->role()->associate($role);
        $user->save();

        return back()->with('alert-success', 'User created!');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', ['user' => $user, 'roles' => Role::all()]);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => [
                'required', 'string', 'email', 'max:255',
                Rule::unique('users')->ignore($user->id)
            ],
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|integer|exists:roles,id'
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $role = Role::find($request['role_id']);

        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);

        $user->role()->associate($role);
        $user->save();

        return back()->with('alert-success', 'User updated!');
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if($user->isRoot() || Auth::user()->id == $user->id)
            return abort(404);

        $user->delete();
        return redirect()->route('users.index');
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        if($user->isRoot() || Auth::user()->id == $user->id)
            return abort(404);
        return view('users.delete', ['user' => $user]);
    }

    public function show_profile($id)
    {
      $photo = UserPhoto::where('user_id', $id)->get();
      // var_dump(sizeof($photo));
      if (sizeof($photo) < 1)
      {
        $array = [];
        // $t = new stdClass();
        // $t->download_path = "upload/qnphPujlGCECztQN9cOdsAR07xwZKOcgmsGNjTzB.png";
        // return $t;
        $array[0]['download_path'] = "upload/qnphPujlGCECztQN9cOdsAR07xwZKOcgmsGNjTzB.png";
        return $array;
      }

      return $photo;
    }

    public function test()
    {
      
    }
}
