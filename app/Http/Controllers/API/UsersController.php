<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Role;
use App\Http\Resources\Users as UsersResource;

class UsersController extends Controller
{
    public function index()
    {
        return UsersResource::collection(User::all());
    }
}
