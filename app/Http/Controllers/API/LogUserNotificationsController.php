<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LogUserNotification;
use App\Item;
use Illuminate\Support\Facades\DB;

class LogUserNotificationsController extends Controller
{
    //
    public function index() {
        $notifications = LogUserNotification
            ::with(["log_item.item" => function($q) {
                $q->where("user_id", "!=", auth()->user()->id)->withTrashed();
            }])
            ->with("log_item.item.user.photo")
            ->orderBy("created_at", "DESC")
            ->where("user_id", auth()->user()->id)
            ->whereRaw('
                !(updated_at <= NOW() - INTERVAL 1 DAY and `read` = 1) AND (DATE(created_at) <= CURDATE())
            ')
            ->limit(15)
            ->get();
        return $notifications;
    }

    public function flag_read($log_user_notification_id) {
        $notification = LogUserNotification::where("user_id", auth()->user()->id)
            ->where("id", $log_user_notification_id)
            ->first();
        $notification->read = 1;
        $notification->save();
        return $notification;
    }

    public function all_user_notification() {
        $notifications = LogUserNotification
            ::with(["log_item.item" => function($q) {
                $q->where("user_id", "!=", auth()->user()->id)->withTrashed();
            }])
            ->with("log_item.item.user.photo")
            ->orderBy("created_at", "DESC")
            ->where("user_id", auth()->user()->id)
            ->paginate(15);
        return $notifications;
    }

    public function count_user_notification() {
        $count = LogUserNotification
            ::with(["log_item.item" => function($q) {
                $q->where("user_id", "!=", auth()->user()->id)->withTrashed();
            }])
            ->where("user_id", auth()->user()->id)
            ->where("read", 0)->get()->count();

        return response()->json([
            "total_unread" => $count
        ], 200);
    }

}
