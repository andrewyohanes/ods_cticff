<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subject;
use App\Http\Resources\Subjects as SubjectsResource;

class SubjectsController extends Controller
{
    public function index()
    {
            return SubjectsResource::collection(Subject::all());
    }


    public function create()
    {

    }

    public function edit()
    {

    }
}
