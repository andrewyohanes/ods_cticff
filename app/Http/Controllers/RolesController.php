<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Role;

class RolesController extends Controller
{
    public function index()
    {
        return view('roles.index', ['roles' => Role::all()]);
    }
}
