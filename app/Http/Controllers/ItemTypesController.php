<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\ItemType;

class ItemTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('role', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item_types.index', ['item_types' => ItemType::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item_types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $country = ItemType::create([
            'name' => $request['name'],
            'description' => $request['description'] ? $request['description'] : ""
        ]);

        return back()->with('alert-success', 'New category created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item_type = ItemType::findOrFail($id);
        return view('item_types.edit', ['item_type' => $item_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);

        if($validator->fails()) {
            return back()
                ->with('alert-danger', 'Form error!')
                ->withErrors($validator)
                ->withInput();
        }

        $item_type = ItemType::findOrFail($id);
        $item_type->name = $request['name'];
        $item_type->description = $request['description'] ? $request['description'] : "";
        $item_type->save();

        return back()->with('alert-success', 'Category data saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ItemType::findOrFail($id);
        $category->delete();
        return redirect()->route('categories.index');
    }

    public function delete($id)
    {
        $category = ItemType::findOrFail($id);
        return view('item_types.delete', ['category' => $category]);
    }
}
