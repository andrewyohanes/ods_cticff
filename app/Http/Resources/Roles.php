<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Roles extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            "id" => $this->id,
            "name" => $this->name,
            "complete_name" => $this->complete_name,
            "description" => $this->description,
            //"created_at" => $this->created_at,
            //"updated_at" => $this->updated_at,
        ];
    }
}
