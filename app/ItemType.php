<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    public function items()
    {
        return $this->hasMany('App\Item', 'item_type_id', 'id');
    }
}
