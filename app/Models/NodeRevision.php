<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 16 Jan 2018 14:26:55 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NodeRevision
 * 
 * @property int $nid
 * @property int $vid
 * @property int $uid
 * @property string $title
 * @property string $body
 * @property string $teaser
 * @property string $log
 * @property int $timestamp
 * @property int $format
 *
 * @package App\Models
 */
class NodeRevision extends Eloquent
{
	protected $connection = 'mysql_cticff';
	protected $primaryKey = 'vid';
	public $timestamps = false;

	protected $casts = [
		'nid' => 'int',
		'uid' => 'int',
		'timestamp' => 'int',
		'format' => 'int'
	];

	protected $fillable = [
		'nid',
		'uid',
		'title',
		'body',
		'teaser',
		'log',
		'timestamp',
		'format'
	];
}
