<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemCountry
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $country_id
 * @property int $item_id
 * 
 * @property \App\Models\Country $country
 * @property \App\Models\Item $item
 *
 * @package App\Models
 */
class ItemCountry extends Eloquent
{
	protected $casts = [
		'country_id' => 'int',
		'item_id' => 'int'
	];

	protected $fillable = [
		'country_id',
		'item_id'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class);
	}

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}
}
