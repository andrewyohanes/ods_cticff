<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:10:39 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Node
 *
 * @property int $nid
 * @property int $vid
 * @property string $type
 * @property string $language
 * @property string $title
 * @property int $uid
 * @property int $status
 * @property int $created
 * @property int $changed
 * @property int $comment
 * @property int $promote
 * @property int $moderate
 * @property int $sticky
 * @property int $tnid
 * @property int $translate
 *
 * @package App\Models
 */
class Node extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $table = 'node';
	protected $primaryKey = 'nid';
    public $web = 'http://coraltriangleinitiative.org/';
	public $timestamps = false;

	protected $casts = [
		'vid' => 'int',
		'uid' => 'int',
		'status' => 'int',
        'created' => 'datetime',
        'changed' => 'datetime',
		'comment' => 'int',
		'promote' => 'int',
		'moderate' => 'int',
		'sticky' => 'int',
		'tnid' => 'int',
		'translate' => 'int'
	];

	protected $fillable = [
		'vid',
		'type',
		'language',
		'title',
		'uid',
		'status',
        'created',
        'changed',
		'comment',
		'promote',
		'moderate',
		'sticky',
		'tnid',
		'translate'
	];

    public function files() {
        return $this->hasManyThrough(\App\Models\File::class, \App\Models\Upload::class, 'nid', 'fid', 'nid', 'fid');
    }

    public function covers() {
        return $this->hasManyThrough(\App\Models\File::class, \App\Models\ContentFieldImage::class, 'nid', 'fid', 'nid', 'field_image_fid');
    }

    public function countries() {
        return $this->hasManyThrough(\App\Models\TermDatum::class, \App\Models\TermNode::class, 'nid', 'tid', 'nid', 'tid')->where('term_data.vid', 6);
    }

    public function subjects() {
        return $this->hasManyThrough(\App\Models\TermDatum::class, \App\Models\TermNode::class, 'nid', 'tid', 'nid', 'tid')->where('term_data.vid', 2);
    }

    public function categories() {
        return $this->hasManyThrough(\App\Models\TermDatum::class, \App\Models\TermNode::class, 'nid', 'tid', 'nid', 'tid')->where('term_data.vid', 5);
    }

    public function authors() {
        return $this->hasMany(\App\Models\ContentTypeDocument::class, 'nid', 'nid');
    }

    public function documents() {
        return $this->hasMany(\App\Models\NodeRevision::class, 'nid', 'nid');
    }

}
