<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Role
 * 
 * @property int $id
 * @property string $name
 * @property string $complete_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Role extends Eloquent
{
	protected $fillable = [
		'name',
		'complete_name',
		'description'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class);
	}
}
