<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:16:32 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContentFieldImage
 *
 * @property int $vid
 * @property int $nid
 * @property int $field_image_fid
 * @property int $field_image_list
 * @property string $field_image_data
 * @property int $delta
 *
 * @package App\Models
 */
class ContentFieldImage extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $table = 'content_field_image';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'vid' => 'int',
		'nid' => 'int',
		'field_image_fid' => 'int',
		'field_image_list' => 'int',
		'delta' => 'int'
	];

	protected $fillable = [
		'nid',
		'field_image_fid',
		'field_image_list',
		'field_image_data'
	];
}
