<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemTag
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $item_id
 * @property int $tag_id
 * 
 * @property \App\Models\Item $item
 * @property \App\Models\Tag $tag
 *
 * @package App\Models
 */
class ItemTag extends Eloquent
{
	protected $casts = [
		'item_id' => 'int',
		'tag_id' => 'int'
	];

	protected $fillable = [
		'item_id',
		'tag_id'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}

	public function tag()
	{
		return $this->belongsTo(\App\Models\Tag::class);
	}
}
