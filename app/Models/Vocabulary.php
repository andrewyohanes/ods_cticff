<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:15:44 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Vocabulary
 *
 * @property int $vid
 * @property string $name
 * @property string $description
 * @property string $help
 * @property int $relations
 * @property int $hierarchy
 * @property int $multiple
 * @property int $required
 * @property int $tags
 * @property string $module
 * @property int $weight
 *
 * @package App\Models
 */
class Vocabulary extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $table = 'vocabulary';
	protected $primaryKey = 'vid';
	public $timestamps = false;

	protected $casts = [
		'relations' => 'int',
		'hierarchy' => 'int',
		'multiple' => 'int',
		'required' => 'int',
		'tags' => 'int',
		'weight' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'help',
		'relations',
		'hierarchy',
		'multiple',
		'required',
		'tags',
		'module',
		'weight'
	];
}
