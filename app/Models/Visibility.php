<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Visibility
 *
 * @property int $id
 * @property int $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $item_id
 *
 * @property \App\Models\Item $item
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class Visibility extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $casts = [
		'type' => 'int',
		'item_id' => 'int'
	];

	protected $fillable = [
		'type',
		'item_id'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}

	public function items()
	{
		return $this->hasMany(\App\Models\Item::class);
	}

	public function users()
	{
		return $this->belongsToMany(\App\Models\User::class, 'visibility_users')
					->withPivot('id')
					->withTimestamps();
	}
}
