<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemFile
 *
 * @property int $id
 * @property string $path
 * @property string $filename
 * @property string $file_id
 * @property string $token
 * @property string $filetype
 * @property string $download_path
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $item_id
 * @property bool $deleted
 *
 * @property \App\Models\Item $item
 *
 * @package App\Models
 */
class ItemFile extends Eloquent
{
	protected $casts = [
		'item_id' => 'int',
		'deleted' => 'bool'
	];

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'path',
		'filename',
		'file_id',
		'token',
		'filetype',
		'download_path',
		'description',
		'item_id',
		'deleted'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}

	public function item_files()
	{
		return $this->belongsToMany(\App\Models\Item::class);
	}
}
