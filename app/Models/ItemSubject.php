<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:07:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemSubject
 * 
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $item_id
 * @property int $subject_id
 * 
 * @property \App\Models\Item $item
 * @property \App\Models\Subject $subject
 *
 * @package App\Models
 */
class ItemSubject extends Eloquent
{
	protected $casts = [
		'item_id' => 'int',
		'subject_id' => 'int'
	];

	protected $fillable = [
		'item_id',
		'subject_id'
	];

	public function item()
	{
		return $this->belongsTo(\App\Models\Item::class);
	}

	public function subject()
	{
		return $this->belongsTo(\App\Models\Subject::class);
	}
}
