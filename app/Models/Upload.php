<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:13:40 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Upload
 *
 * @property int $fid
 * @property int $nid
 * @property int $vid
 * @property string $description
 * @property int $list
 * @property int $weight
 *
 * @package App\Models
 */
class Upload extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $table = 'upload';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'fid' => 'int',
		'nid' => 'int',
		'vid' => 'int',
		'list' => 'int',
		'weight' => 'int'
	];

	protected $fillable = [
		'nid',
		'description',
		'list',
		'weight'
	];
}
