<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 14 Jan 2018 21:13:47 +0700.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class File
 *
 * @property int $fid
 * @property int $uid
 * @property string $filename
 * @property string $filepath
 * @property string $filemime
 * @property int $filesize
 * @property int $status
 * @property int $timestamp
 *
 * @package App\Models
 */
class File extends Eloquent
{
    protected $connection = 'mysql_cticff';
	protected $primaryKey = 'fid';
	public $timestamps = false;

	protected $casts = [
		'uid' => 'int',
		'filesize' => 'int',
		'status' => 'int',
		'timestamp' => 'int'
	];

	protected $fillable = [
		'uid',
		'filename',
		'filepath',
		'filemime',
		'filesize',
		'status',
		'timestamp'
	];
}
