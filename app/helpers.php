<?php

if(!function_exists('find_file_by_name')) {
    function find_file_by_name($files, $name) {
        foreach($files as $file) {
            if($file->filename === $name) {
                return $file;
            }
        }
        return null;
    }
}

if(!function_exists('extract_file_name')) {
    function extract_file_name($files) {
        $data = [];
        foreach($files as $file) {
            array_push($data, $file->filename);
        }
        natsort($data);
        return $data;
    }
}